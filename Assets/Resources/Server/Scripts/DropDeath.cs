using UnityEngine;
using System.Collections;

public class DropDeath : MonoBehaviour {

	private GameObject gameManager;
	private GameManager gameManagerScript;

	
	
	
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			gameManager = GameObject.FindGameObjectWithTag("GameManager");	
			gameManagerScript = gameManager.GetComponent<GameManager>();		
			gameManagerScript.abilityDamage(other.gameObject, 9999);
		}
	}
}
