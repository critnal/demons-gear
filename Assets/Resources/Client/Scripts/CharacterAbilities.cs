using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterAbilities : MonoBehaviour {
	
	public CharacterEquipment equipment;
	
	public int lmbAbilityIndex = 0;
	public int rmbAbilityIndex = 0;
	private int selectedAbilityIndex = 9;
	
	public List<Protocol.Ability> equippedAbilities = new List<Protocol.Ability>();
	
	public Protocol.MovementAbility equippedMovementAbility = Protocol.MovementAbility.Null;
	

    float attackTime = 0.13f;
    float startAttackTime = 0.0f;
    float timer = 0.0f;
    string attackSelected = "2hSword";
	
	void Start() {
		equipment = GetComponent<CharacterEquipment>();
	}

	void Update () {

    		if(Input.GetKeyDown(KeyCode.Tab)){
			if(attackSelected == "2hSword"){
			attackSelected = "Bow";
			}
			else if(attackSelected == "Bow"){
			attackSelected = "2hSword";
			}
		}
		
		if(Input.GetKey(KeyCode.Mouse0)){
			gameObject.GetComponent<Animate>().isAttacking = true;
			gameObject.GetComponent<Animate>().selectedAttack = attackSelected; // put this in the attack selection
			float attackTime = 0.13f;
			gameObject.GetComponent<Animate>().attackReleased = false;
		}
		else {
			gameObject.GetComponent<Animate>().isAttacking = false;
		}
		
		if(Input.GetKeyUp(KeyCode.Mouse0)){
			gameObject.GetComponent<Animate>().attackReleased = true;
			startAttackTime = Time.time;
			timer = Time.time + attackTime;
		}
		
		if(timer <= Time.time){
			gameObject.GetComponent<Animate>().attackReleased = false;
			}
		
		
		
		// Check for selected ability
		if (Input.GetKeyDown(KeyCode.Alpha1) && equippedAbilities.Count >= 1) {
			selectedAbilityIndex = 0;
		} 
		else if (Input.GetKeyDown(KeyCode.Alpha2) && equippedAbilities.Count >= 2) {
			selectedAbilityIndex = 1;
		} 
		else if (Input.GetKeyDown(KeyCode.Alpha3) && equippedAbilities.Count >= 3) {
			selectedAbilityIndex = 2;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha4) && equippedAbilities.Count >= 4) {
			selectedAbilityIndex = 3;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha5) && equippedAbilities.Count >= 5) {
			selectedAbilityIndex = 4;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha6) && equippedAbilities.Count >= 6) {
			selectedAbilityIndex = 5;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha7) && equippedAbilities.Count >= 7) {
			selectedAbilityIndex = 6;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha8) && equippedAbilities.Count >= 8) {
			selectedAbilityIndex = 7;
		}
		else if (Input.GetKeyDown(KeyCode.Alpha9) && equippedAbilities.Count >= 9) {
			selectedAbilityIndex = 8;
		}
		
		// Assign the selected ability to a mouse button
		if (selectedAbilityIndex != 9) {
			if (Input.GetMouseButtonDown(0)) {
				lmbAbilityIndex = selectedAbilityIndex;
				selectedAbilityIndex = 9;
			}
			if (Input.GetMouseButtonDown(1)) {
				rmbAbilityIndex = selectedAbilityIndex;
				selectedAbilityIndex = 9;
			}
		}
	}
	
	


	
	
}
