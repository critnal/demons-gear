using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightningBolt : MonoBehaviour {

	public bool isClient = false;
	public float damageModifier = 0;
	public long attackerID = 0;
	private GameObject gameManager;
	private GameManager gameManagerScript;

	// Use this for initialization
	void Awake () {
		Destroy(this.gameObject, 5f);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
//		Vector3 newVelocity = this.transform.up * 10;
//		this.rigidbody.velocity = new Vector3(newVelocity.x, newVelocity.y, 0);
		
//		foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player")) {
//			Physics.IgnoreCollision(this.GetComponent<SphereCollider>(), player.GetComponent<CapsuleCollider>());
//		}
	}
	
	
	void OnCollisionEnter(Collision collision) {
		
		ContactPoint contact = collision.contacts[0];
		if (contact.otherCollider.tag == "Player") {
			
			if (GameObject.Find("GameManager") == null) {
				return;
			}
			
//			long otherID = other.GetComponent<NetworkCharacter>().ID;			
//			if (attackerID == otherID) {
//				return;
//			}
			
			gameManager = GameObject.Find("GameManager");	
			gameManagerScript = gameManager.GetComponent<GameManager>();		
			gameManagerScript.abilityDamage(contact.otherCollider.gameObject, 100);
		}
	}
}
