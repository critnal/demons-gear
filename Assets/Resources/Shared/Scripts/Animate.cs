using UnityEngine;
using System.Collections;
using SmoothMoves;
using System;


public class Animate : MonoBehaviour {
	
	public bool onGround = true;
	public Vector2 moveDirection = Vector2.zero;
	private BoneAnimation PlayerAnimation;
    public bool isAttacking = false;
    public string selectedAttack;
    public bool attackReleased = false;
	
	private bool lastOnGround = true;
	
	void Start() {
		try {
			PlayerAnimation = GetComponentInChildren<BoneAnimation>();
		} catch (Exception) {
			PlayerAnimation = null;
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {
		
		if (PlayerAnimation != null) {
			
			if(moveDirection == Vector2.right) {
				PlayerAnimation.transform.eulerAngles = new Vector3(0, 0, 0);
			} else if (moveDirection == -Vector2.right) {
				PlayerAnimation.transform.eulerAngles = new Vector3(0, 180, 0);
			}
			
			if (onGround != lastOnGround) {
				if (onGround == false) {
					PlayerAnimation.CrossFade("2hJump");
				} else {
					PlayerAnimation.CrossFade("2hLand");
				}
			} else {
				if (onGround == false) {
					PlayerAnimation.CrossFade("2hFloat");
				} else {
					if(moveDirection == Vector2.zero) {
						PlayerAnimation.CrossFade("2hIdle", 0.16f);
					} else {
						PlayerAnimation.Play("2hRun");
					}
				}
			}
			if (gameObject.GetComponent<Character>().Mine) {
	            if (isAttacking){
	                if (selectedAttack == "2hSword")
	                {
	                    PlayerAnimation.CrossFade("2hMeleeCharge");
	                }
	                else if (selectedAttack == "Bow")
	                {
	                    PlayerAnimation.Play("BowCharge");
	                }
	            }
	            if (attackReleased){
	                if (selectedAttack == "2hSword")
	                {
	                    PlayerAnimation.Play("2hMeleeLightStable");
	                }
	                if (selectedAttack == "Bow")
	                {
	                    PlayerAnimation.Play("BowAttackStable");
	                }
	            }
			}
			
			lastOnGround = onGround;
				
		}
		
	}
	
	public void MeleeAttack() {
		PlayerAnimation.Play("2hMeleeLightStable");
	}
	
	public void BowAttack() {
		PlayerAnimation.Play("BowAttackStable");
	}
	
}
