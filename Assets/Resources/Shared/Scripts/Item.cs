using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item : MonoBehaviour {
	
	public string Name;
	public Protocol.Slot Slot;
	public List<string> Bones;
	public List<Protocol.Ability> Abilities;
	public Protocol.MovementAbility MovementAbility;
	
	public Item() {
		Name = "";
		Slot = Protocol.Slot.Null;
		Abilities = new List<Protocol.Ability>() {Protocol.Ability.Null};
		MovementAbility = Protocol.MovementAbility.Null;
	}

	public Item(Protocol.Slot Slot, string Name, List<Protocol.Ability> Abilities, Protocol.MovementAbility MovementAbility) {
		this.Slot = Slot;
		this.Name = Name;
		this.Abilities = Abilities;
		this.MovementAbility = MovementAbility;
	}
}
