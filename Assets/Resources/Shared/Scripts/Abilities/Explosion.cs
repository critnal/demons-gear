using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public bool isClient = true;
	public float damageModifier = 0;
	private GameObject gameManager;
	private GameManager gameManagerScript;

	// Use this for initialization
	void Start () {
		
		if (GameObject.FindGameObjectWithTag("GameManager") != null) {
			isClient = false;
		}
		Destroy(this.gameObject, 0.5f);
		
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	
	void OnTriggerEnter(Collider other) {
		
		if (other.tag == "Player") {
			if (GameObject.FindGameObjectWithTag("GameManager") == null) {
				print("hit");
				return;
			}
			
			gameManager = GameObject.FindGameObjectWithTag("GameManager");	
			gameManagerScript = gameManager.GetComponent<GameManager>();	
			
			gameManagerScript.abilityDamage(other.gameObject, Mathf.RoundToInt(100 * damageModifier));
		}
	}
}
