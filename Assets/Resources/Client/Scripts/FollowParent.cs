using UnityEngine;
using System.Collections;

public class FollowParent : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y, transform.position.z);
	}
}
