using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmoothMoves;

public class CharacterEquipment : MonoBehaviour {
	
	public CharacterAbilities characterAbilities;
	
	public List<Item> EquippedItems = new List<Item>() {new Item(), new Item(), new Item(), new Item(), new Item(), new Item(), };
	
	
	public void AddItem(Item item) {
		EquippedItems[(int)item.Slot] = item;		
	}
	
	public void RemoveItem(Protocol.Slot slot) {
		EquippedItems[(int)slot] = new Item();
	}
	
	public Item GetItem(Protocol.Slot slot) {
		return EquippedItems[(int)slot];
	}
	
	
	
	public void SetEquippedAbilities() {
		List<Protocol.Ability> abilities = new List<Protocol.Ability>();
		foreach (Item item in EquippedItems) {
				foreach (Protocol.Ability ability in item.Abilities) {
					if (ability != Protocol.Ability.Null) {
						abilities.Add(ability);
					}
				}
			
		}
		characterAbilities.equippedAbilities = abilities;
	}
	
	public void SetEquippedMovementAbility() {
		characterAbilities.equippedMovementAbility = EquippedItems[(int)Protocol.Slot.Boots].MovementAbility;
	}
	
	
	public List<Item> headItems = new List<Item>();
	public List<Item> chestItems = new List<Item>();
	public List<Item> legItems = new List<Item>();
	public List<Item> gloveItems = new List<Item>();
	public List<Item> bootItems = new List<Item>();
	public List<Item> weaponItems = new List<Item>();
	
	public class BoneList {
		public List<string> boneList;
		public Protocol.Slot slot;
		public BoneList(Protocol.Slot slot, List<string> list) {
			this.slot = slot;
			this.boneList = list;
		}
	}
	
	private static BoneList headBones = new BoneList(Protocol.Slot.Head, new List<string>() {"Head"});
	private static BoneList chestBones = new BoneList(Protocol.Slot.Chest, new List<string>() {"Chest", "LeftUpperArm", "RightUpperArm"});
	private static BoneList legBones = new BoneList(Protocol.Slot.Legs, new List<string>() {"LeftLeg", "RightLeg"});
	private static BoneList gloveBones = new BoneList(Protocol.Slot.Gloves, new List<string>() {"LeftHand", "LeftForeArm", "RightHand", "RightForeArm"});
	private static BoneList bootBones = new BoneList(Protocol.Slot.Boots, new List<string>() {"LeftBoot", "RightBoot"});
	private static BoneList weaponBones = new BoneList(Protocol.Slot.Weapon, new List<string>() {"Weapon"});
	
	public List<BoneList> boneList = new List<BoneList>() { headBones, chestBones, legBones, gloveBones, bootBones, weaponBones };
	
	
	
	public void toggleEquipment(byte key) {
		if (key == (byte)Protocol.Command.T) {
			Item defaultItem = weaponItems[0];
			Item nextItem = nextItemInSlotList(Protocol.Slot.Weapon, weaponItems);
			EquippedItems[(int)Protocol.Slot.Weapon] = nextItem;
			
			updateStuff(defaultItem, nextItem);
		}
		
		if (key == (byte)Protocol.Command.G) {
			Item defaultItem = headItems[0];
			Item nextItem = nextItemInSlotList(Protocol.Slot.Head, headItems);
			EquippedItems[(int)Protocol.Slot.Head] = nextItem;
			
			updateStuff(defaultItem, nextItem);
		}
		
		if (key == (byte)Protocol.Command.B) {
			Item defaultItem = chestItems[0];
			Item nextItem = nextItemInSlotList(Protocol.Slot.Chest, chestItems);
			EquippedItems[(int)Protocol.Slot.Chest] = nextItem;
			
			updateStuff(defaultItem, nextItem);
		}
		
		if (key == (byte)Protocol.Command.Y) {
			Item defaultItem = legItems[0];
			Item nextItem = nextItemInSlotList(Protocol.Slot.Legs, legItems);
			EquippedItems[(int)Protocol.Slot.Legs] = nextItem;
			
			updateStuff(defaultItem, nextItem);
		}
		
		if (key == (byte)Protocol.Command.H) {
			Item defaultItem = gloveItems[0];
			Item nextItem = nextItemInSlotList(Protocol.Slot.Gloves, gloveItems);
			EquippedItems[(int)Protocol.Slot.Gloves] = nextItem;
			
			updateStuff(defaultItem, nextItem);
		}
		
		if (key == (byte)Protocol.Command.N) {
			Item defaultItem = bootItems[0];
			Item nextItem = nextItemInSlotList(Protocol.Slot.Boots, bootItems);
			EquippedItems[(int)Protocol.Slot.Boots] = nextItem;
			
			updateStuff(defaultItem, nextItem);
		}
	}
	
	private void updateStuff(Item defaultItem, Item nextItem) {
		SetEquippedAbilities();
		SetEquippedMovementAbility();
		updateSlotTexture(defaultItem, nextItem);
	}
	
	
	private Item nextItemInSlotList(Protocol.Slot slot, List<Item> list) {
		
		Item currentItemInSlot = EquippedItems[(int)slot];
	
		int currentListIndex = -1;
		for (int i = 1; i < list.Count; i++) {
			if (currentItemInSlot.Name == list[i].Name) {
				currentListIndex = i;
			}
		}
		currentListIndex++;
		
		if (currentListIndex >= list.Count) {
			currentListIndex = 1;
		}
		
		return list[currentListIndex];
	}	
	
	private void updateSlotTexture(Item fromItem, Item toItem) {
		
		BoneAnimation characterAnimation = GetComponentInChildren<BoneAnimation>();
		
		List<string> bonesToUpdate = getBonesToUpdate(fromItem);
		
		foreach (string bone in bonesToUpdate) {
			string fromTexture = bone + fromItem.Name;
			string toTexture = bone + toItem.Name;
			
			characterAnimation.SwapTexture(
			bone,
			fromTexture,
			bone,
			toTexture
			);
		}		
	}
	
	private List<string> getBonesToUpdate(Item item) {
		foreach (BoneList boneSet in boneList) {
			if (boneSet.slot == item.Slot) {
				return boneSet.boneList;
			}
		}
		return null;
	}
	
	
	
	void Start() {
		
		characterAbilities = GetComponent<CharacterAbilities>();
		
		// WEAPONS
		weaponItems.Add(new Item(
			Protocol.Slot.Weapon,
			"2hLongSword",
			new List<Protocol.Ability>() {Protocol.Ability.Melee},
			Protocol.MovementAbility.Null
			));
		
		weaponItems.Add(new Item(
			Protocol.Slot.Weapon,
			"RibbonSpear",
			new List<Protocol.Ability>() {Protocol.Ability.Melee},
			Protocol.MovementAbility.Null
			));
//		
		weaponItems.Add(new Item(
			Protocol.Slot.Weapon,
			"fireaxe",
			new List<Protocol.Ability>() {Protocol.Ability.Melee},
			Protocol.MovementAbility.Null
			));
		
		weaponItems.Add(new Item(
			Protocol.Slot.Weapon,
			"LongMace",
			new List<Protocol.Ability>() {Protocol.Ability.Melee, Protocol.Ability.Shockwave},
			Protocol.MovementAbility.Null
			));
		
//		weaponItems.Add(new Item(
//			Protocol.Slot.Weapon,
//			"MetalAxe",
//			new List<Protocol.Ability>() {Protocol.Ability.Melee},
//			Protocol.MovementAbility.Null
//			));
		
//		weaponItems.Add(new Item(
//			Protocol.Slot.Weapon,
//			"MetalMace",
//			new List<Protocol.Ability>() {Protocol.Ability.Melee, Protocol.Ability.Shockwave},
//			Protocol.MovementAbility.Null
//			));
		
//		weaponItems.Add(new Item(
//			Protocol.Slot.Weapon,
//			"WoodenSwordv2",
//			new List<Protocol.Ability>() {Protocol.Ability.Melee},
//			Protocol.MovementAbility.Null
//			));
		
		weaponItems.Add(new Item(
			Protocol.Slot.Weapon,
			"Zangetsu",
			new List<Protocol.Ability>() {Protocol.Ability.Melee, Protocol.Ability.PhaseBlade},
			Protocol.MovementAbility.Null
			));
		
		weaponItems.Add(new Item(
			Protocol.Slot.Weapon,
			"Bow",
			new List<Protocol.Ability>() {Protocol.Ability.Ranged, Protocol.Ability.MultiShot},
			Protocol.MovementAbility.Null
			));
		
		weaponItems.Add(new Item(
			Protocol.Slot.Weapon,
			"Staff",
			new List<Protocol.Ability>() {Protocol.Ability.Fireball, Protocol.Ability.LightningBolt},
			Protocol.MovementAbility.Null
			));
		
		
		
//		weaponItems.Add(new Item(
//			Protocol.Slot.Weapon,
//			"Bow",
//			new List<Protocol.Ability>() {Protocol.Ability.Ranged, Protocol.Ability.ExplosiveShot, Protocol.Ability.GhostShot},
//			Protocol.MovementAbility.Null
//			));
//		
		
		
		
		// HEAD
		headItems.Add(new Item(
			Protocol.Slot.Head,
			"Monk",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		headItems.Add(new Item(
			Protocol.Slot.Head,
			"Taurus",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		headItems.Add(new Item(
			Protocol.Slot.Head,
			"Mage",
			new List<Protocol.Ability>() {Protocol.Ability.IcyChill},
			Protocol.MovementAbility.Null
			));
		
		headItems.Add(new Item(
			Protocol.Slot.Head,
			"Pirate",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
		
		headItems.Add(new Item(
			Protocol.Slot.Head,
			"Archer",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
		
		
		
		
		// CHEST
		chestItems.Add(new Item(
			Protocol.Slot.Chest,
			"Monk",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		chestItems.Add(new Item(
			Protocol.Slot.Chest,
			"Taurus",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		chestItems.Add(new Item(
			Protocol.Slot.Chest,
			"Mage",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
		
		chestItems.Add(new Item(
			Protocol.Slot.Chest,
			"Pirate",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		chestItems.Add(new Item(
			Protocol.Slot.Chest,
			"Archer",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		
		
		
		
		// LEGS
		legItems.Add(new Item(
			Protocol.Slot.Legs,
			"Monk",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		legItems.Add(new Item(
			Protocol.Slot.Legs,
			"Taurus",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		legItems.Add(new Item(
			Protocol.Slot.Legs,
			"Mage",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
		
		legItems.Add(new Item(
			Protocol.Slot.Legs,
			"Pirate",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		legItems.Add(new Item(
			Protocol.Slot.Legs,
			"Archer",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
		
		
		
		
		// Gloves
		gloveItems.Add(new Item(
			Protocol.Slot.Gloves,
			"Monk",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		gloveItems.Add(new Item(
			Protocol.Slot.Gloves,
			"Taurus",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		gloveItems.Add(new Item(
			Protocol.Slot.Gloves,
			"Mage",
			new List<Protocol.Ability>() {Protocol.Ability.MagicSpray},
			Protocol.MovementAbility.Null
			));
		
		gloveItems.Add(new Item(
			Protocol.Slot.Gloves,
			"Pirate",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
//		
		gloveItems.Add(new Item(
			Protocol.Slot.Gloves,
			"Archer",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Null
			));
		
		
		
		
		// Boots
		bootItems.Add(new Item(
			Protocol.Slot.Boots,
			"Monk",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Dash
			));
//		
		bootItems.Add(new Item(
			Protocol.Slot.Boots,
			"Taurus",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Dash
			));
//		
		bootItems.Add(new Item(
			Protocol.Slot.Boots,
			"Mage",
			new List<Protocol.Ability>() {Protocol.Ability.Thundercloud},
			Protocol.MovementAbility.Diffuse
			));
		
		bootItems.Add(new Item(
			Protocol.Slot.Boots,
			"Pirate",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Dash
			));
//		
		bootItems.Add(new Item(
			Protocol.Slot.Boots,
			"Archer",
			new List<Protocol.Ability>() {Protocol.Ability.Null},
			Protocol.MovementAbility.Teleport
			));
		
		
			
		
		
		AddItem(weaponItems[1]);
		AddItem(headItems[1]);
		AddItem(chestItems[1]);
		AddItem (legItems[1]);
		AddItem(gloveItems[1]);
		AddItem(bootItems[1]);
		print (EquippedItems[(int)Protocol.Slot.Weapon].Abilities[0].ToString());
	}
	
}
