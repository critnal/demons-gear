using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterPhysics))]
public class NetworkCharacter : MonoBehaviour {
	
	public long ID;
	
	public int HP = 500;
	public int maxHP = 500;
	
	public float abilityStartTime = 0;
	
	// Use this for initialization
	void Start () {
		Debug.Log("New Character class");
	}
	
	void OnGUI() {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
