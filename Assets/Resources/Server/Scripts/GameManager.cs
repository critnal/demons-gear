using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Lidgren.Network;

public class GameManager : MonoBehaviour
{
	NetServer server;
	public GameObject character;
	private float tempTime = 0.0f;
	
	void Start ()
	{
				
		NetPeerConfiguration config = new NetPeerConfiguration ("Game");
		config.Port = 14242;
		config.MaximumConnections = 100;
				
		server = new NetServer (config);
		server.Start ();
		Debug.Log ("Server Started");
	}
	
	void Update ()
	{
		if (Time.time > tempTime + (0.06)) {
			
			foreach (NetConnection conn in server.Connections) {
				GameObject temp = conn.Tag as GameObject;
				
				if (temp != null) {
					CharacterPhysics temp1 = temp.GetComponent<CharacterPhysics> ();
				
					if (temp1 != null) {
						NetOutgoingMessage msg = server.CreateMessage ();
						msg.Write ((byte)Protocol.Command.Sync);
						msg.Write (conn.RemoteUniqueIdentifier);
						temp.GetComponent<CharacterPhysics> ().MakeMovementPacket (ref msg);
						server.SendToAll (msg, NetDeliveryMethod.UnreliableSequenced);
					}
				}
				
				
			}
			tempTime = Time.time;
		}
		
		NetIncomingMessage im;
		while ((im = server.ReadMessage()) != null) {
			// handle incoming message
			switch (im.MessageType) {
			case NetIncomingMessageType.DebugMessage:
			case NetIncomingMessageType.ErrorMessage:
			case NetIncomingMessageType.WarningMessage:
			case NetIncomingMessageType.VerboseDebugMessage:
				string text = im.ReadString ();
				Debug.Log (text);
				break;
			case NetIncomingMessageType.StatusChanged:
				NetConnectionStatus status = (NetConnectionStatus)im.ReadByte ();
				string reason = im.ReadString ();
				Debug.Log (NetUtility.ToHexString (im.SenderConnection.RemoteUniqueIdentifier) + " " + status + ": " + reason);
				
				if (status == NetConnectionStatus.Connected) {
					
					im.SenderConnection.Tag = Instantiate (character);
					GameObject temp = im.SenderConnection.Tag as GameObject;
					temp.GetComponent<NetworkCharacter> ().ID = im.SenderConnection.RemoteUniqueIdentifier;
					temp.transform.position = GameObject.FindGameObjectWithTag("Spawn").transform.position;
					
					
					NetOutgoingMessage msg = server.CreateMessage ();
					msg.Write ((byte)Protocol.Command.Connection);
					msg.Write (im.SenderConnection.RemoteUniqueIdentifier);
					server.SendToAll (msg, im.SenderConnection, NetDeliveryMethod.ReliableUnordered, (int)Protocol.Channel.S2C);
					
					foreach (NetConnection conn in server.Connections) {
						if (conn.RemoteUniqueIdentifier != im.SenderConnection.RemoteUniqueIdentifier) {
										
							NetOutgoingMessage msg1 = server.CreateMessage ();
							msg1.Write ((byte)Protocol.Command.Connection);
							msg1.Write (conn.RemoteUniqueIdentifier);
							server.SendMessage (msg1, im.SenderConnection, NetDeliveryMethod.ReliableUnordered, (int)Protocol.Channel.S2C);
							Debug.Log ("sent player info");
						}
					}				
											
					
				} else if (status == NetConnectionStatus.Disconnected) {
					
					
					NetOutgoingMessage msg2 = server.CreateMessage ();
					msg2.Write ((byte)Protocol.Command.Disconnection);
					msg2.Write (im.SenderConnection.RemoteUniqueIdentifier);
					server.SendToAll (msg2, im.SenderConnection, NetDeliveryMethod.ReliableUnordered, (int)Protocol.Channel.S2C);
					
					GameObject temp = im.SenderConnection.Tag as GameObject;
					Destroy(temp);
					server.Connections.Remove(im.SenderConnection);
					
				}

				break;
			case NetIncomingMessageType.Data:
				byte cmd = im.ReadByte ();
							
				
				GameObject temp1 = im.SenderConnection.Tag as GameObject;
				NetworkCharacter temp = temp1.GetComponent<NetworkCharacter> ();
				
				switch (cmd) {
					
				case (byte)Protocol.Command.LeftDown:
					Debug.Log ("Left Down");
								
					temp.GetComponent<CharacterPhysics> ().LeftDown = true;
								
					break;
				case (byte)Protocol.Command.LeftUp:
					Debug.Log ("Left Up");
								
					temp.GetComponent<CharacterPhysics> ().LeftDown = false;
								
					break;
				case (byte)Protocol.Command.RightDown:
					Debug.Log ("Right Down");
								
					temp.GetComponent<CharacterPhysics> ().RightDown = true;
								
					break;
				case (byte)Protocol.Command.RightUp:
					Debug.Log ("Right Up");
								
					temp.GetComponent<CharacterPhysics> ().RightDown = false;
								
					break;
				case (byte)Protocol.Command.UpDown:
					Debug.Log ("Up Pressed");
								
					temp.GetComponent<CharacterPhysics> ().UpDown = true;
								
					break;
				case (byte)Protocol.Command.UpUp:
					Debug.Log ("Up Pressed");
								
					temp.GetComponent<CharacterPhysics> ().UpDown = false;
								
					break;
				case (byte)Protocol.Command.DownDown:
					Debug.Log ("Down Down");
								
					temp.GetComponent<CharacterPhysics> ().DownDown = true;
								
					break;
				case (byte)Protocol.Command.DownUp:
					Debug.Log ("Down Up");
								
					temp.GetComponent<CharacterPhysics> ().DownDown = false;
								
					break;
					
					
				case (byte)Protocol.Command.Mouse0Down:
					
					if (true) {
						Protocol.Ability ability = (Protocol.Ability)im.ReadInt32();
						Vector2 rot = new Vector2(im.ReadFloat(), im.ReadFloat());
						Vector2 pos = new Vector2(im.ReadFloat(), im.ReadFloat());
						Protocol.Command command = Protocol.Command.Mouse0Down;
						
						GameObject tempPlayer = im.SenderConnection.Tag as GameObject;
						NetworkCharacter networkPlayer = tempPlayer.GetComponent<NetworkCharacter> ();						
						networkPlayer.abilityStartTime = Time.time;
						
						
						NetOutgoingMessage msg2 = server.CreateMessage ();
						msg2.Write ((byte)command);
						msg2.Write ((int)ability);
						msg2.Write (rot.x);
						msg2.Write (rot.y);
						msg2.Write (pos.x);
						msg2.Write (pos.y);
						msg2.Write (networkPlayer.abilityStartTime);
						msg2.Write (networkPlayer.GetComponent<NetworkCharacter>().ID);
						
						server.SendToAll(msg2, NetDeliveryMethod.ReliableOrdered);
												
						networkPlayer.GetComponent<Abilities>().doAbility(ability, command, pos, rot, networkPlayer.abilityStartTime, im.SenderConnection.RemoteUniqueIdentifier);
					}
					break;
					
				case (byte)Protocol.Command.Mouse0Up:
					
					if (true) {
						Protocol.Ability ability = (Protocol.Ability)im.ReadInt32();
						Vector2 rot = new Vector2(im.ReadFloat(), im.ReadFloat());
						Vector2 pos = new Vector2(im.ReadFloat(), im.ReadFloat());
						Protocol.Command command = Protocol.Command.Mouse0Up;
						
						GameObject tempPlayer = im.SenderConnection.Tag as GameObject;
						NetworkCharacter networkPlayer = tempPlayer.GetComponent<NetworkCharacter> ();						
						float totalAbilityTime = Time.time - networkPlayer.abilityStartTime;
						
						NetOutgoingMessage msg2 = server.CreateMessage ();
						msg2.Write ((byte)command);
						msg2.Write ((int)ability);
						msg2.Write (rot.x);
						msg2.Write (rot.y);
						msg2.Write (pos.x);
						msg2.Write (pos.y);
						msg2.Write (totalAbilityTime);
						msg2.Write (networkPlayer.GetComponent<NetworkCharacter>().ID);
						
						server.SendToAll(msg2, NetDeliveryMethod.ReliableOrdered);
						
						networkPlayer.GetComponent<Abilities>().doAbility(ability, command, pos, rot, totalAbilityTime, im.SenderConnection.RemoteUniqueIdentifier);
					}
					break;
					
					
					case (byte)Protocol.Command.Mouse1Down:
					
					if (true) {
						Protocol.Ability ability = (Protocol.Ability)im.ReadInt32();
						Vector2 rot = new Vector2(im.ReadFloat(), im.ReadFloat());
						Vector2 pos = new Vector2(im.ReadFloat(), im.ReadFloat());
						Protocol.Command command = Protocol.Command.Mouse1Down;
						
						GameObject tempPlayer = im.SenderConnection.Tag as GameObject;
						NetworkCharacter networkPlayer = tempPlayer.GetComponent<NetworkCharacter> ();						
						networkPlayer.abilityStartTime = Time.time;
						
						
						NetOutgoingMessage msg2 = server.CreateMessage ();
						msg2.Write ((byte)command);
						msg2.Write ((int)ability);
						msg2.Write (rot.x);
						msg2.Write (rot.y);
						msg2.Write (pos.x);
						msg2.Write (pos.y);
						msg2.Write (networkPlayer.abilityStartTime);
						msg2.Write (networkPlayer.GetComponent<NetworkCharacter>().ID);
						
						server.SendToAll(msg2, NetDeliveryMethod.ReliableOrdered);
												
						networkPlayer.GetComponent<Abilities>().doAbility(ability, command, pos, rot, networkPlayer.abilityStartTime, im.SenderConnection.RemoteUniqueIdentifier);
					}
					break;
					
				case (byte)Protocol.Command.Mouse1Up:
					
					if (true) {
						Protocol.Ability ability = (Protocol.Ability)im.ReadInt32();
						Vector2 rot = new Vector2(im.ReadFloat(), im.ReadFloat());
						Vector2 pos = new Vector2(im.ReadFloat(), im.ReadFloat());
						Protocol.Command command = Protocol.Command.Mouse1Up;
						
						GameObject tempPlayer = im.SenderConnection.Tag as GameObject;
						NetworkCharacter networkPlayer = tempPlayer.GetComponent<NetworkCharacter> ();						
						float totalAbilityTime = Time.time - networkPlayer.abilityStartTime;
						
						NetOutgoingMessage msg2 = server.CreateMessage ();
						msg2.Write ((byte)command);
						msg2.Write ((int)ability);
						msg2.Write (rot.x);
						msg2.Write (rot.y);
						msg2.Write (pos.x);
						msg2.Write (pos.y);
						msg2.Write (totalAbilityTime);
						msg2.Write (networkPlayer.GetComponent<NetworkCharacter>().ID);
						
						server.SendToAll(msg2, NetDeliveryMethod.ReliableOrdered);
						
						networkPlayer.GetComponent<Abilities>().doAbility(ability, command, pos, rot, totalAbilityTime, im.SenderConnection.RemoteUniqueIdentifier);
					}
					break;
									
					
					case (byte)Protocol.Command.SpaceDown:
					
					if (true) {
						Protocol.Command command = Protocol.Command.SpaceDown;
						Protocol.MovementAbility moveAbility = (Protocol.MovementAbility)im.ReadInt32();
						Vector2 pos = new Vector2(im.ReadFloat(), im.ReadFloat());	
						GameObject tempPlayer = im.SenderConnection.Tag as GameObject;
						
						NetOutgoingMessage msg2 = server.CreateMessage ();
						msg2.Write ((byte)command);
						msg2.Write ((int)moveAbility);
						msg2.Write (pos.x);
						msg2.Write (pos.y);
						msg2.Write ((tempPlayer.GetComponent<NetworkCharacter>().ID));
						
						server.SendToAll(msg2, NetDeliveryMethod.ReliableOrdered);
						
						
						
						tempPlayer.GetComponent<MovementAbilities>().doMovementAbility(moveAbility, command, pos);
					}
					break;
					
					case (byte)Protocol.Command.SpaceUp:
					
					if (true) {
						Protocol.Command command = Protocol.Command.SpaceUp;
						Protocol.MovementAbility moveAbility = (Protocol.MovementAbility)im.ReadInt32();
						Vector2 pos = new Vector2(im.ReadFloat(), im.ReadFloat());	
						GameObject tempPlayer = im.SenderConnection.Tag as GameObject;
						
						NetOutgoingMessage msg2 = server.CreateMessage ();
						msg2.Write ((byte)command);
						msg2.Write ((int)moveAbility);
						msg2.Write (pos.x);
						msg2.Write (pos.y);
						msg2.Write ((tempPlayer.GetComponent<NetworkCharacter>().ID));
						
						server.SendToAll(msg2, NetDeliveryMethod.ReliableOrdered);
						
						
						
						tempPlayer.GetComponent<MovementAbilities>().doMovementAbility(moveAbility, command, pos);
					}
					break;
					
				case (byte)Protocol.Command.T :
				case (byte)Protocol.Command.G :
				case (byte)Protocol.Command.B :
				case (byte)Protocol.Command.Y :
				case (byte)Protocol.Command.H :
				case (byte)Protocol.Command.N :
					
					GameObject tempPlayer = im.SenderConnection.Tag as GameObject;
					
					NetOutgoingMessage msg = server.CreateMessage ();
					msg.Write (cmd);
					msg.Write ((tempPlayer.GetComponent<NetworkCharacter>().ID));
					server.SendToAll(msg, NetDeliveryMethod.ReliableOrdered);
					
					break;
				}
							
				break;
			default:
				Debug.Log ("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes " + im.DeliveryMethod + "|" + im.SequenceChannel);
				break;
			}
		}
		
	}
	
	void OnApplicationQuit ()
	{
		server.Shutdown ("");
	}
	
	
	
	
	public void abilityDamage(GameObject damagedPlayer, int damageDealt) {
  
  		damagedPlayer.GetComponent<NetworkCharacter>().HP -= damageDealt;
  
		  NetOutgoingMessage msg = server.CreateMessage ();
		  msg.Write ((byte)Protocol.Command.Damage);
		  msg.Write (damagedPlayer.GetComponent<NetworkCharacter>().ID);
		  msg.Write ((int) damageDealt);
		  
		  server.SendToAll(msg, NetDeliveryMethod.ReliableOrdered);
		  
		  if (damagedPlayer.GetComponent<NetworkCharacter>().HP < 1) {
		//         
			   GameObject[] spawns = GameObject.FindGameObjectsWithTag("Spawn");
			   damagedPlayer.transform.position = spawns[Random.Range(0, spawns.Length)].transform.position;
			   damagedPlayer.transform.rigidbody.velocity = Vector3.zero;
			   
			   damagedPlayer.GetComponent<NetworkCharacter>().HP = damagedPlayer.GetComponent<NetworkCharacter>().maxHP;
			   
			   NetOutgoingMessage msg4 = server.CreateMessage ();
			   msg4.Write ((byte)Protocol.Command.Respawn);
			   msg4.Write (damagedPlayer.GetComponent<NetworkCharacter>().ID);
			   msg4.Write ((int) damagedPlayer.GetComponent<NetworkCharacter>().maxHP);
			   
			   server.SendToAll(msg4, NetDeliveryMethod.ReliableOrdered);
  		}
 }
	
}
