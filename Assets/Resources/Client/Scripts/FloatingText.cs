using UnityEngine;
using System.Collections;

public class FloatingText : MonoBehaviour {
		
	Vector2 position = Vector2.zero;
	
	static public void Instantiation(string text, Color color, Vector2 pos) {
		
		GameObject n = new GameObject();
		n.AddComponent<GUIText>();
		n.AddComponent<FloatingText>();
		n.transform.position = new Vector3(pos.x, pos.y, 0);
		
		n.GetComponent<FloatingText>().position = pos;
		
		n.guiText.fontStyle = FontStyle.Bold;
		n.guiText.alignment = TextAlignment.Center;
		n.guiText.anchor = TextAnchor.MiddleCenter;
		n.guiText.text = text;
		n.guiText.material.color = color;
	}
	
	// Update is called once per frame
	void OnGUI () {
		
		guiText.material.color = new Color(guiText.material.color.r, guiText.material.color.g, guiText.material.color.b, guiText.material.color.a - (Time.deltaTime * 0.2f));
		
		position = new Vector2(position.x, position.y + (Time.deltaTime * 0.035f));
		//Vector3 me = Camera.main.WorldToViewportPoint(Client.Me.transform.position);
		
		guiText.transform.position = new Vector3(position.x, position.y, 0);
		
		if (guiText.material.color.a <= 0) {
			Destroy(gameObject);
		}
		
	}
}
