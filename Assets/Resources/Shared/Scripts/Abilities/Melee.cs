using UnityEngine;
using System.Collections;

public class Melee : MonoBehaviour {

	public bool isClient = true;
	public long attackerID = 0;
	public GameObject attackerObject;
	private GameObject gameManager;
	private GameManager gameManagerScript;

	// Use this for initialization
	void Start () {
		
		if (GameObject.Find("GameManager") != null) {
			isClient = false;
		}
		
		if (isClient) {
			Destroy(this.gameObject);
		} else {
			Destroy(this.gameObject, 0.2f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player" && other.gameObject != attackerObject) {
			
			if (GameObject.Find("GameManager") == null) {
				return;
			}
			
//			long otherID = other.GetComponent<NetworkCharacter>().ID;			
//			if (attackerID == otherID) {t
//				return;
//			}
			
			
			
			gameManager = GameObject.FindGameObjectWithTag("GameManager");	
			gameManagerScript = gameManager.GetComponent<GameManager>();		
			gameManagerScript.abilityDamage(other.gameObject, 100);
		}
	}
}
