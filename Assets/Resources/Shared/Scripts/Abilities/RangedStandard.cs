using UnityEngine;
using System.Collections;

public class RangedStandard : MonoBehaviour {
	
	public bool isClient = false;
	public float damageModifier = 0;
	public long attackerID = 0;
	private GameObject gameManager;
	private GameManager gameManagerScript;

	// Use this for initialization
	void Awake () {
		Destroy(this.gameObject, 20f);
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			
			Destroy (this.gameObject);
			
			if (GameObject.Find("GameManager") == null) {
				return;
			}
			
//			long otherID = other.GetComponent<NetworkCharacter>().ID;			
//			if (attackerID == otherID) {
//				return;
//			}
			
			gameManager = GameObject.Find("GameManager");	
			gameManagerScript = gameManager.GetComponent<GameManager>();		
			gameManagerScript.abilityDamage(other.gameObject, 100);
		}
		
		else if (other.tag != "RangedStandard") {
			Destroy (this.gameObject);
		}
	}
}