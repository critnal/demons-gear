using UnityEngine;
using System.Collections;

public class ExplosionAnimation : MonoBehaviour {
	
	public float duration = 0.5f;
	
	public Transform animation1, animation2;	
	
	public float layerRotateSpeed = 60;
	public float layerSpeedDiff = 2f;
	
	public GameObject explosionTrailPrefab;
	
	public float trailSpeed = 1000f;
	public float maxTrails = 5;
	public float trailLineDuration = 0f;

	// Use this for initialization
	void Start () {		
		animation1 = transform.Find("Animation1");
		animation2 = transform.Find("Animation2");
		
		float animation1StartAngle = Random.Range(0f, 360f);
		float animation2StartAngle = Random.Range(0f, 360f);
		animation1.transform.Rotate(transform.forward, animation1StartAngle);
		animation2.transform.Rotate(transform.forward, animation2StartAngle);
		
		explosionTrailPrefab = Resources.Load("Shared/Prefabs/Abilities/ExplosionTrail") as GameObject;
		trailLineDuration = duration;
		
		float startAngle = Random.Range(0f, 360f);
		float currentAngle = 0f;
		
		// Spawn explosion trails		
		for (int count = 0; count < maxTrails; count++) {
			
			GameObject explosionTrailInstance = Instantiate(explosionTrailPrefab, transform.position, Quaternion.identity) as GameObject;
			
			currentAngle = startAngle + count * (360 / maxTrails) * Random.Range(0.8f, 1.1f);			
			explosionTrailInstance.transform.Rotate(explosionTrailInstance.transform.forward, currentAngle);
			explosionTrailInstance.rigidbody.AddForce(explosionTrailInstance.transform.up * trailSpeed);
			
			Destroy(explosionTrailInstance, duration);
		}
		
		
		Destroy(this.gameObject, duration);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// Rotate the two explosion textures in opposite directions
		animation1.transform.Rotate(transform.forward, layerRotateSpeed * Time.fixedDeltaTime);
		animation2.transform.Rotate(transform.forward, -layerRotateSpeed * layerSpeedDiff * Time.fixedDeltaTime);
	}
}
