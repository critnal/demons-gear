using UnityEngine;
using System.Collections;

public class IcyChill : MonoBehaviour {
	
	public bool isClient = false;
//	public float abilityDuration = 4f;
	public float endTime = 0f;
	public long attackerID = 0;
	private GameObject gameManager;
	private GameManager gameManagerScript;

	// Use this for initialization
	void Start () {
		Destroy(this.gameObject, 3f);
		endTime = Time.time + 3f;
	}
	
	void FixedUpdate() {
		
	}
	
	
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			if (GameObject.Find("GameManager") == null) {
				return;
			}
			
			gameManager = GameObject.Find("GameManager");	
			gameManagerScript = gameManager.GetComponent<GameManager>();		
			
						
//			long otherID = other.GetComponent<NetworkCharacter>().ID;
//			
//			if (attackerID == otherID) {
//				return;
//			}
			
			float currentDamage = (endTime - Time.time) * 60;
			
			gameManagerScript.abilityDamage(other.gameObject, Mathf.RoundToInt(currentDamage));
		}
		
		else {
		}
	}
}