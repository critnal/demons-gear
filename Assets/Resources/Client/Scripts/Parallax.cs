using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {
	
	private GameObject camera;
	private float moveDistance;
	float distance;
		
	// Use this for initialization
	void Start () {
		moveDistance = (100 - transform.position.z) * 0.001f;
		distance = this.transform.position.z;
	}
	
	// Update is called once per frame
	void FixedUpdate () {				
		camera = GameObject.FindGameObjectWithTag("MainCamera");
		Vector3 moveFrom = this.transform.position;
		Vector3 moveTo = new Vector3(-(camera.transform.position.x * moveDistance), transform.position.y, transform.position.z);
		this.transform.position = Vector3.Lerp(moveFrom, moveTo, Time.fixedDeltaTime * 100);
	}
}
