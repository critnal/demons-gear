using UnityEngine;
using System.Collections;
using Lidgren.Network;
using System.Net;
using System.Threading;
using System.IO;
using System.Collections.Generic;

public class Client : MonoBehaviour
{
	
	public LineRenderer lineRenderer;
	
	public GameObject CharacterPrefab;
	public GameObject CameraPrefab;
	
	static public Character Me;
	public static NetClient client;
	List<Character> players = new List<Character>();
	
	void Awake () 
	{
		lineRenderer = GetComponent<LineRenderer>();
	}
	
	void Start ()
	{		
		
		string host = "123.243.182.112";
		int port = 14242;
		
		string[] arguments = System.Environment.GetCommandLineArgs();
		
  		if (arguments.Length > 1) {
  			if (arguments[1] != null) {
  				host = arguments[1];
  			}
  		}
  	
  		if (arguments.Length > 2) {
  			if (arguments[2] != null) {
  				port = System.Int32.Parse(arguments[2]);
  			}
  		}
		
		
		
		NetPeerConfiguration config = new NetPeerConfiguration ("Game");
		client = new NetClient (config);
		client.Start ();
		//client.Connect ("123.243.182.112", 14242);
		client.Connect (host, port);
	}
	
	void Update ()
	{
		
		
		if (Input.GetMouseButtonDown(0)) {			
			CharacterAbilities abilities = Me.GetComponent<CharacterAbilities>();
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 pos = new Vector2(Me.transform.position.x, Me.transform.position.y);
			Vector2 rot = (new Vector2(mousePos.x, mousePos.y) - pos).normalized;
			
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.Mouse0Down);
			outmsg.Write ((int)(abilities.equippedAbilities[abilities.lmbAbilityIndex]));
			outmsg.Write (rot.x);
			outmsg.Write (rot.y);
			outmsg.Write (pos.x);
			outmsg.Write (pos.y);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
			
		} else if (Input.GetMouseButtonUp(0)) {			
			CharacterAbilities abilities = Me.GetComponent<CharacterAbilities>();
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 pos = new Vector2(Me.transform.position.x, Me.transform.position.y);
			Vector2 rot = (new Vector2(mousePos.x, mousePos.y) - pos).normalized;
			//print (abilities.equippedAbilities[abilities.lmbAbilityIndex].ToString());
			
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.Mouse0Up);
			outmsg.Write ((int)(abilities.equippedAbilities[abilities.lmbAbilityIndex]));
			outmsg.Write (rot.x);
			outmsg.Write (rot.y);
			outmsg.Write (pos.x);
			outmsg.Write (pos.y);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		}
		
		if (Input.GetMouseButtonDown(1)) {			
			CharacterAbilities abilities = Me.GetComponent<CharacterAbilities>();
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 pos = new Vector2(Me.transform.position.x, Me.transform.position.y);
			Vector2 rot = (new Vector2(mousePos.x, mousePos.y) - pos).normalized;
			
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.Mouse1Down);
			outmsg.Write ((int)(abilities.equippedAbilities[abilities.rmbAbilityIndex]));
			outmsg.Write (rot.x);
			outmsg.Write (rot.y);
			outmsg.Write (pos.x);
			outmsg.Write (pos.y);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
			
		} else if (Input.GetMouseButtonUp(1)) {			
			CharacterAbilities abilities = Me.GetComponent<CharacterAbilities>();
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 pos = new Vector2(Me.transform.position.x, Me.transform.position.y);
			Vector2 rot = (new Vector2(mousePos.x, mousePos.y) - pos).normalized;
			print (abilities.equippedAbilities[abilities.lmbAbilityIndex].ToString());
			
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.Mouse1Up);
			outmsg.Write ((int)(abilities.equippedAbilities[abilities.rmbAbilityIndex]));
			outmsg.Write (rot.x);
			outmsg.Write (rot.y);
			outmsg.Write (pos.x);
			outmsg.Write (pos.y);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		}
		
		
		if (Input.GetKeyDown(KeyCode.Space)) {
			CharacterAbilities abilities = Me.GetComponent<CharacterAbilities>();
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 pos = new Vector2(mousePos.x, mousePos.y);
			print(abilities.equippedMovementAbility.ToString());
			
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.SpaceDown);
			outmsg.Write ((int)abilities.equippedMovementAbility);
			outmsg.Write (pos.x);
			outmsg.Write (pos.y);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		} else if (Input.GetKeyUp(KeyCode.Space)) {
			CharacterAbilities abilities = Me.GetComponent<CharacterAbilities>();
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 pos = new Vector2(mousePos.x, mousePos.y);
			
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.SpaceUp);
			outmsg.Write ((int)abilities.equippedMovementAbility);
			outmsg.Write (pos.x);
			outmsg.Write (pos.y);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		}
		
		
		
		if (Input.GetKeyDown (KeyCode.D)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.RightDown);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		} else if (Input.GetKeyUp (KeyCode.D)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.RightUp);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		}
		
		if (Input.GetKeyDown (KeyCode.A)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.LeftDown);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		} else if (Input.GetKeyUp (KeyCode.A)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.LeftUp);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		}
		
		if (Input.GetKeyDown (KeyCode.W)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.UpDown);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		} else if (Input.GetKeyUp (KeyCode.W)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.UpUp);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		}
		
		if (Input.GetKeyDown (KeyCode.S)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.DownDown);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		} else if (Input.GetKeyUp (KeyCode.S)) {
			NetOutgoingMessage outmsg = client.CreateMessage ();
			outmsg.Write ((byte)Protocol.Command.DownUp);
			client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
		}
		
		if (Input.GetKeyDown (KeyCode.T)) {
			newItemMessage(Protocol.Command.T);
		} else if (Input.GetKeyDown (KeyCode.G)) {
			newItemMessage(Protocol.Command.G);
		} else if (Input.GetKeyDown (KeyCode.B)) {
			newItemMessage(Protocol.Command.B);
		} else if (Input.GetKeyDown (KeyCode.Y)) {
			newItemMessage(Protocol.Command.Y);
		} else if (Input.GetKeyDown (KeyCode.H)) {
			newItemMessage(Protocol.Command.H);
		} else if (Input.GetKeyDown (KeyCode.N)) {
			newItemMessage(Protocol.Command.N);
		}
		
		
		NetIncomingMessage msg;
		while ((msg = client.ReadMessage()) != null) {
			// handle incoming message
			switch (msg.MessageType) {
			case NetIncomingMessageType.DebugMessage:
			case NetIncomingMessageType.ErrorMessage:
			case NetIncomingMessageType.WarningMessage:
			case NetIncomingMessageType.VerboseDebugMessage:
				string text = msg.ReadString ();
				Debug.Log (text);
				break;
			case NetIncomingMessageType.StatusChanged:
				NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte ();
				string reason = msg.ReadString ();
				Debug.Log (status.ToString () + ": " + reason);
				
				if (status == NetConnectionStatus.Connected) {					
					GameObject temp = Instantiate (CharacterPrefab) as GameObject;
					
					GameObject[] spawns = GameObject.FindGameObjectsWithTag("Spawn");
			   		temp.transform.position = spawns[Random.Range(0, spawns.Length)].transform.position;
					Character tempCharacter = temp.GetComponent<Character>();
					
					GameObject cam = Instantiate (CameraPrefab) as GameObject;
					cam.transform.parent = temp.transform;
					
					tempCharacter.ID = client.UniqueIdentifier;
					Debug.Log("My Player: " + tempCharacter.ID);
					tempCharacter.Mine = true;
					Me = tempCharacter;
					
					players.Add(Me);
					newItemMessage(Protocol.Command.T);
				} else if (status == NetConnectionStatus.Disconnected) {
					Application.Quit();
				}				
				
				break;
			case NetIncomingMessageType.Data:
						
				byte cmd = msg.ReadByte ();
							
				switch (cmd) {
				case (byte)Protocol.Command.Sync:
					
					Character tempPlayer = players.Find(i => i.ID == msg.PeekInt64());
					if (tempPlayer != null) {
						msg.ReadInt64();
						tempPlayer.UseSyncPacket(ref msg);
					}
					
					break;
				case (byte)Protocol.Command.Connection:
					
					GameObject temp = Instantiate (CharacterPrefab) as GameObject;
					Character tempCharacter = temp.GetComponent<Character>();
					
					tempCharacter.ID = msg.ReadInt64();
					Debug.Log("New Player: " + tempCharacter.ID);
					players.Add(tempCharacter);
					
					break;
					
				case (byte)Protocol.Command.Disconnection:
					
					Character tempPlayer1 = players.Find(i => i.ID == msg.PeekInt64());
					if (tempPlayer1 != null) {
						Destroy(tempPlayer1.gameObject);
						players.Remove(tempPlayer1);
					}
					
					break;
					
					
				case (byte)Protocol.Command.Mouse0Down:
					
//					Ray ray = new Ray(new Vector3(pos.x, pos.y, 0), new Vector3(rot.x, rot.y, 0));
//					
//					Debug.Log(rot.ToString());
//					Debug.Log(pos.ToString());
					
//					switch(ability) {
//					case Protocol.Ability.Melee:
						//For melee attacks
					
					
//						lineRenderer.SetWidth(1.5f, 1.5f);
//						lineRenderer.SetColors(Color.red, Color.red);
//					
//						lineRenderer.SetPosition(0, ray.origin);
//						lineRenderer.SetPosition(1, ray.GetPoint(2.5f));
//						break;
						
//					case Protocol.Ability.Ranged:
//						lineRenderer.SetWidth(0.3f, 0.2f);
//						lineRenderer.SetColors(Color.green, Color.green);
//						lineRenderer.SetPosition(0, ray.origin);
//						lineRenderer.SetPosition(1, ray.GetPoint(35f));
						
						// FIREBALL
//						GameObject abilityPrefab = Resources.Load("Client/Prefabs/Abilities/Fireball") as GameObject;
//						Vector3 targetPos = new Vector3(rot.x, rot.y, 0);
//						GameObject abilityVisual = Instantiate(abilityPrefab, ray.origin, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
//						abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 500);
						
						// EXPLOSION
//						GameObject abilityPrefab = Resources.Load("Shared/Prefabs/Abilities/Explosion") as GameObject;
//						Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
//						targetPos.z = 0;
//						GameObject abilityVisual = Instantiate(abilityPrefab, targetPos, Quaternion.identity) as GameObject;
						
						// LIGHTNING BOLT
//						GameObject abilityPrefab = Resources.Load("Client/Prefabs/Abilities/LightningBolt") as GameObject;
//						Vector3 targetPos = new Vector3(rot.x, rot.y, 0);
//						GameObject abilityVisual = Instantiate(abilityPrefab, ray.origin, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
//						abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 1500);
						
						// ICY CHILL
//						GameObject abilityPrefab = Resources.Load("Client/Prefabs/Abilities/IcyChill") as GameObject;
//						Vector3 targetPos = new Vector3(rot.x, rot.y, 0);
//						GameObject abilityVisual = Instantiate(abilityPrefab, ray.origin, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
//						abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 500);
						
						// RANGED STANDARD
//						GameObject abilityPrefab = Resources.Load("Client/Prefabs/Abilities/RangedStandard") as GameObject;
//						Vector3 targetPos = new Vector3(rot.x, rot.y, 0);
//						GameObject abilityVisual = Instantiate(abilityPrefab, ray.origin, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
//						abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 1000);
						
						// MULTI SHOT
//						GameObject abilityPrefab = Resources.Load("Client/Prefabs/Abilities/RangedStandard") as GameObject;
//						Vector3 targetPos = new Vector3(rot.x, rot.y, 0);
//						GameObject abilityVisual = Instantiate(abilityPrefab, ray.origin, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
//						abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 1000);
						
						// GHOST SHOT
//						GameObject abilityPrefab = Resources.Load("Client/Prefabs/Abilities/GhostShot") as GameObject;
//						Vector3 targetPos = new Vector3(rot.x, rot.y, 0);
//						GameObject abilityVisual = Instantiate(abilityPrefab, ray.origin, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
//						abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 1000);
						
						// EXPLOSIVE SHOT
//						GameObject abilityPrefab = Resources.Load("Client/Prefabs/Abilities/ExplosiveShot") as GameObject;
//						Vector3 targetPos = new Vector3(rot.x, rot.y, 0);
//						GameObject abilityVisual = Instantiate(abilityPrefab, ray.origin, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
//						abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 1000);
						
						
						
						
					break;
						
						
				case (byte)Protocol.Command.Mouse0Up :
					if (true) {
						Protocol.Ability ability = (Protocol.Ability)msg.ReadInt32();
						Vector2 rot = new Vector2(msg.ReadFloat(), msg.ReadFloat());
						Vector2 pos = new Vector2(msg.ReadFloat(), msg.ReadFloat());
						float chargeDuration = msg.ReadFloat();
						long id = msg.ReadInt64();
						
						Character tempPlayer2 = players.Find(i => i.ID == id);
						
						tempPlayer2.GetComponent<Abilities>().doAbility(ability, Protocol.Command.Mouse0Up, pos, rot, chargeDuration, id);
					}
					
					break;
					
				case (byte)Protocol.Command.Mouse1Up :
					if (true) {
						Protocol.Ability ability = (Protocol.Ability)msg.ReadInt32();
						Vector2 rot = new Vector2(msg.ReadFloat(), msg.ReadFloat());
						Vector2 pos = new Vector2(msg.ReadFloat(), msg.ReadFloat());
						float chargeDuration = msg.ReadFloat();
						long id = msg.ReadInt64();
						
						Character tempPlayer2 = players.Find(i => i.ID == id);
						
						tempPlayer2.GetComponent<Abilities>().doAbility(ability, Protocol.Command.Mouse1Up, pos, rot, chargeDuration, id);
					}
					
					break;
					
					
					
				case (byte)Protocol.Command.SpaceDown :
					if (true) {
						Protocol.Command command = Protocol.Command.SpaceDown;
						Protocol.MovementAbility moveAbility = (Protocol.MovementAbility)msg.ReadInt32();
						Vector2 pos = new Vector2(msg.ReadFloat(), msg.ReadFloat());
						long id = msg.ReadInt64();
						
						Character tempPlayer2 = players.Find(i => i.ID == id);
						
						tempPlayer2.GetComponent<MovementAbilities>().doMovementAbility(moveAbility, command, pos);
					}
					
					break;
					
				case (byte)Protocol.Command.SpaceUp :
					if (true) {
						Protocol.Command command = Protocol.Command.SpaceUp;
						Protocol.MovementAbility moveAbility = (Protocol.MovementAbility)msg.ReadInt32();
						Vector2 pos = new Vector2(msg.ReadFloat(), msg.ReadFloat());
						long id = msg.ReadInt64();
						
						Character tempPlayer2 = players.Find(i => i.ID == id);
						
						tempPlayer2.GetComponent<MovementAbilities>().doMovementAbility(moveAbility, command, pos);
					}
					
					break;
					
				
					
				case (byte)Protocol.Command.T :
				case (byte)Protocol.Command.G :
				case (byte)Protocol.Command.B :
				case (byte)Protocol.Command.Y :
				case (byte)Protocol.Command.H :
				case (byte)Protocol.Command.N :
					
					if (true) {
						long id = msg.ReadInt64();
						
						Character tempPlayer2 = players.Find(i => i.ID == id);
						tempPlayer2.gameObject.GetComponent<CharacterEquipment>().toggleEquipment(cmd);
					}
					
					break;
					
					
					
					
					
					
					
					
					
					
					
				case (byte)Protocol.Command.Damage:
					
					long id = msg.ReadInt64();
					
					Character tempPlayer2 = players.Find(i => i.ID == id);
					if (tempPlayer2 != null) {
					
						int damage = msg.ReadInt32();
						
						tempPlayer2.HP -= damage;
						
						
						
						FloatingText.Instantiation(damage.ToString(), Color.red, Camera.main.WorldToViewportPoint(tempPlayer2.transform.position));
					}
				
					break;
					
					
				case (byte)Protocol.Command.Respawn:
					print ("RESPAWN");
					
					long id1 = msg.ReadInt64();
					
					Character tempPlayer3 = players.Find(i => i.ID == id1);
					if (tempPlayer3 != null) {
						
						tempPlayer3.MaxHp = msg.ReadInt32();
						tempPlayer3.HP = tempPlayer3.MaxHp;
					}
				
					break;
					
				default :
					break;
					
				}
			break;
				
			default:
				Debug.Log ("Unhandled type: " + msg.MessageType + " " + msg.LengthBytes + " bytes");
				break;
			}
		}
	}
	
	void OnApplicationQuit() {
		client.Shutdown("");
	}
		
		
	void newItemMessage(Protocol.Command command) {
		NetOutgoingMessage outmsg = client.CreateMessage ();
		outmsg.Write ((byte)command);
		client.SendMessage (outmsg, NetDeliveryMethod.ReliableSequenced, (int)Protocol.Channel.C2S);
	}
	
}
