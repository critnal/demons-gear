using UnityEngine;
using System.Collections;

public class MovementAbilities : MonoBehaviour {
	
	private float dashTime = 0f;
	private bool dashing = false;
	
	void FixedUpdate() {
		if (Time.time > dashTime && dashing) {
			dash (Protocol.Command.SpaceUp, new Vector2(0, 0));
		}
	}

	public void doMovementAbility(Protocol.MovementAbility movementAbility, Protocol.Command command, Vector2 target) {
	
		switch (movementAbility) {
		case Protocol.MovementAbility.Dash :
			dash (command, target);
			break;
				
		case Protocol.MovementAbility.Teleport :
			teleport (command, target);
			break;
			
		case (Protocol.MovementAbility.Diffuse) :
			diffuse(command, target);
			break;
			
			
		default :
			break;
		}
		
	}
	
	private void dash(Protocol.Command command, Vector2 target) {
		
		if (command == Protocol.Command.SpaceDown) {			
			int direction = 0;
			if (this.gameObject.GetComponent<CharacterPhysics>().RightDown) {
				direction = 1;
			} else if (this.gameObject.GetComponent<CharacterPhysics>().LeftDown) {
				direction = -1;
			}
			
			if (direction != 0) {
				this.gameObject.GetComponent<CharacterPhysics>().isDoingMovementAbility = true;
				this.gameObject.rigidbody.velocity = new Vector3(50 * direction, 0, 0);
				dashTime = Time.time + 0.2f;
				dashing = true;
			}
			
		} else {
			if (this.gameObject.rigidbody.velocity.x > 20) {
				this.gameObject.rigidbody.velocity = new Vector3(20, 0, 0);
			} else if (this.gameObject.rigidbody.velocity.x < -20) {
				this.gameObject.rigidbody.velocity = new Vector3(-20, 0, 0);
			}
			dashing = false;
			this.gameObject.GetComponent<CharacterPhysics>().isDoingMovementAbility = false;
			
		}
	}
	
	
	private void teleport(Protocol.Command command, Vector2 target) {
		if (command == Protocol.Command.SpaceDown) {
			
			this.gameObject.transform.position = new Vector3(target.x, target.y, 0);		
			this.gameObject.rigidbody.velocity = new Vector3(0, 0, 0);
		} else {
		}
	}
	
	private void diffuse(Protocol.Command command, Vector2 target) {
		if (command == Protocol.Command.SpaceDown) {
			this.gameObject.GetComponent<CapsuleCollider>().isTrigger = true;			
		} else {
			this.gameObject.GetComponent<CapsuleCollider>().isTrigger = false;	
		}
	}
}
