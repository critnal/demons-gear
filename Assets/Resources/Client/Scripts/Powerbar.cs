using UnityEngine;
using System.Collections;

public class Powerbar : MonoBehaviour {
	
	public Texture powerBarBackground;
	public Texture powerBarFill;
	public float maxPower = 100.0f;
	public float currentPower = 0.0f;
	public float chargeSpeed = 10.0f;
	public int powerBarWidth = 100;
	public int powerBarHeight = 20;
	private float percentPower;
	
	private Vector2 position;
	
	void Start(){
	}
	
	// Update is called once per frame
	void Update () {
		position.x = Input.mousePosition.x;
		position.y = Screen.height - Input.mousePosition.y;
		
		// Power cannot below 0
		if (currentPower < 0.0f){
			currentPower = 0.0f;
		}
		// Power cannot be above its Maximum
		else if (currentPower > maxPower){
			currentPower = maxPower;
		}
		
		if (Input.GetKey(KeyCode.Mouse0)){
			currentPower += chargeSpeed * Time.deltaTime;
		}
		
		if (Input.GetKeyUp(KeyCode.Mouse0)){
			currentPower = 0.0f;
		}
		
		percentPower = currentPower / maxPower;
	
	}
	
	void OnGUI(){
		GUI.DrawTexture(new Rect(position.x - (powerBarWidth/2), position.y - powerBarHeight, powerBarWidth, powerBarHeight), powerBarBackground);
		GUI.DrawTexture(new Rect(position.x - (powerBarWidth/2), position.y - powerBarHeight, powerBarWidth * percentPower, powerBarHeight), powerBarFill);
	}
}
