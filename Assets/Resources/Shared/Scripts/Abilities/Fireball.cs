using UnityEngine;
using System.Collections;
using Lidgren.Network;

public class Fireball : MonoBehaviour {
	
	public bool isClient = false;
	public float damageModifier = 0;
	public long attackerID = 0;
	private GameObject explosionPrefab;
	private GameObject gameManager;
	private GameManager gameManagerScript;

	// Use this for initialization
	void Awake () {
		Destroy(this.gameObject, 20f);
		explosionPrefab = Resources.Load("Shared/Prefabs/Abilities/Explosion") as GameObject;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			explode();
			
//			long otherID = other.GetComponent<NetworkCharacter>().ID;
//			
//			if (attackerID == otherID) {
//				return;
//			}
//			
//			
//			
//			if (isClient) {
//				return;
//			}
		}
		
		else {
			explode();
		}
	}
	
	void explode() {
		Destroy (this.gameObject);
		GameObject explosion = Instantiate(explosionPrefab, this.transform.position, Quaternion.identity) as GameObject;
		explosion.GetComponent<Explosion>().damageModifier = damageModifier;
		if (isClient) {
			explosion.GetComponent<Explosion>().isClient = true;
		}
	}
}
