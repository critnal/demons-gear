using System.Collections;

public class Protocol {

	public enum Command : byte {
		Sync,
		Connection,
		Disconnection,
		LeftDown,
		LeftUp,
		RightDown,
		RightUp,
		UpDown,
		UpUp,
		DownUp,
		DownDown,
		Mouse0Down,
		Mouse0Up,
		Mouse1Down,
		Mouse1Up,
		SpaceDown,
		SpaceUp,
		Damage,
		Respawn,
		T,
		G,
		B,
		Y,
		H,
		N,
		None
	}
	
	public enum Channel : int {
		S2C,
		C2S
	}
	
	public enum Slot : byte {
		Weapon,
		Head,
		Chest,
		Legs,
		Gloves,
		Boots,
		Null
	}
	
	public enum Ability : int {
		Melee,
		Shockwave,
		RisingSplash,
		PhaseBlade,
		CripplingStrike,
		Ranged,
		MultiShot,
		LineShot,
		ExplosiveShot,
		SmogShot,
		GhostShot,
		Fireball,
		MagicSpray,
		LightningBolt,
		Thundercloud,
		IcyChill,
		Null
	}
	
	public enum MovementAbility : int {
		Diffuse,
		Teleport,
		Dash,
		Null
	}
	
}
