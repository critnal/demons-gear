using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {
	
	public float width;
	public float hoverOffset;
	
	public Texture HealthBackground;
	public Texture HealthGreen;
	public Texture HealthRed;
	public Texture HealthMarker;
	
	public int MarkerGap;
	
	public float DecaySpeed;
	
	public int CurrentHealth;
	public int MaxHealth;
	
	public Vector2 pos;
	
	
	private float DamagedHealth;
	private int PreviousHealth;
	
	void Start() {
		DamagedHealth = MaxHealth;
	}
	
	void Update() {
		
		MaxHealth = gameObject.GetComponent<Character>().MaxHp;
		CurrentHealth = gameObject.GetComponent<Character>().HP;
		
		if (DamagedHealth > CurrentHealth) {
			DamagedHealth -= (Time.deltaTime * (DecaySpeed * ((float)MaxHealth) / (float)(HealthBackground.width - 2)));
		}		
	}
	
	void LateUpdate() {
		
		
		pos = Camera.mainCamera.WorldToScreenPoint(transform.position);
		pos.x -= (HealthBackground.width / width) / 2;
		pos.y = Screen.height - pos.y - hoverOffset;
	}
	
	void OnGUI() {
		
		int GreenEnd = Mathf.RoundToInt((((float)CurrentHealth / (float)MaxHealth) * (float)(HealthBackground.width - 2)) / width);
		
		GUI.DrawTexture(new Rect(pos.x, pos.y, HealthBackground.width / width, HealthBackground.height), HealthBackground);
		GUI.DrawTexture(new Rect(pos.x + 1, pos.y + 1, GreenEnd, HealthGreen.height), HealthGreen);
		
		float MarkerAmount = (float)CurrentHealth / (float)MarkerGap;
		
		for (int i = 1; i < MarkerAmount; i++) {
			GUI.DrawTexture(new Rect(pos.x + 1+ Mathf.RoundToInt((i * (MarkerGap * ((float)(HealthBackground.width - 2) / (float)MaxHealth)))/width), pos.y + 1, 1, HealthMarker.height), HealthMarker);  
		}
		
		if (DamagedHealth > CurrentHealth) {
			GUI.DrawTexture(new Rect(pos.x + 1 + GreenEnd, pos.y + 1, (((Mathf.RoundToInt(DamagedHealth) - (float)CurrentHealth) / (float)MaxHealth) * (float)(HealthBackground.width - 2) / width), HealthRed.height), HealthRed);
		}
		
		if (PreviousHealth > CurrentHealth) {
			DamagedHealth = PreviousHealth;
		}
		
		PreviousHealth = CurrentHealth;
		
	}
	
}
