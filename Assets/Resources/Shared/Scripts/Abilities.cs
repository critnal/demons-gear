using UnityEngine;
using System.Collections;
using System;

public class Abilities : MonoBehaviour {

	public void doAbility(Protocol.Ability ability, Protocol.Command command, Vector2 position, Vector2 rotation, float currentDuration, long attackerID) {
		
		switch(ability) {
			
		case Protocol.Ability.Melee :
			melee(command, position, rotation, currentDuration, attackerID);
			break;
			
		case Protocol.Ability.Fireball :
			fireball(command, position, rotation, currentDuration, attackerID);
			break;
			
		case Protocol.Ability.Ranged :
			ranged(command, position, rotation, currentDuration, attackerID);
			break;
		
		case Protocol.Ability.MultiShot :
			multiShot(command, position, rotation, currentDuration, attackerID);
			break;
			
		case Protocol.Ability.IcyChill :
			icyChill(command, position, rotation, currentDuration, attackerID);
			break;
			
		case Protocol.Ability.LightningBolt :
			lightningBolt(command, position, rotation, currentDuration, attackerID);
			break;
			
		default :
			break;
			
		}
			
	}
	
	
	
	
	private void melee(Protocol.Command command, Vector2 position, Vector2 rotation,  float currentDuration, long attackerID) {
		
		if (command == Protocol.Command.Mouse0Down || command == Protocol.Command.Mouse1Down) {			
			
		} else if (command == Protocol.Command.Mouse0Up || command == Protocol.Command.Mouse1Up) {
			GameObject abilityPrefab = Resources.Load("Shared/Prefabs/Abilities/Melee") as GameObject;
			
			float direction = -1f;
			if (rotation.x >= 0) {
				direction = 1f;
			}
			
			float offset = direction * 2.5f;
			
			Vector3 targetPos = new Vector3(this.transform.position.x + offset, this.transform.position.y, 0);
			
			
					
			
//			Vector3 offsetPosition = new Vector3(position.x + offset.x, position.y + offset.y, 0);
			
			try {
			gameObject.GetComponent<Animate>().MeleeAttack();
			} catch (Exception) {
				
			}
			
			GameObject abilityVisual = Instantiate(abilityPrefab, targetPos, Quaternion.identity) as GameObject;
			abilityVisual.transform.localScale = new Vector3(abilityVisual.transform.localScale.x * direction, abilityVisual.transform.localScale.y, abilityVisual.transform.localScale.z);
			
//			print (rigidbody.velocity.x);
//			abilityVisual.rigidbody.velocity = new Vector3(rigidbody.velocity.x * 100, rigidbody.velocity.y, 0);
			
			abilityVisual.GetComponent<Melee>().attackerID = attackerID;
			abilityVisual.GetComponent<Melee>().attackerObject = this.gameObject;
		}
	}
	
	private void fireball(Protocol.Command command, Vector2 position, Vector2 rotation,  float currentDuration, long attackerID) {
		
		if (command == Protocol.Command.Mouse0Down || command == Protocol.Command.Mouse1Down) {			
			
		} else if (command == Protocol.Command.Mouse0Up || command == Protocol.Command.Mouse1Up) {
			GameObject abilityPrefab = Resources.Load("Shared/Prefabs/Abilities/Fireball") as GameObject;
			Vector3 targetPos = new Vector3(rotation.x, rotation.y, 0);
			
			Vector3 offset = targetPos * 3.5f;
			Vector3 offsetPosition = new Vector3(position.x + offset.x, position.y + offset.y, 0);
			
			try {
			gameObject.GetComponent<Animate>().MeleeAttack();
			} catch (Exception) {
				
			}
			
			GameObject abilityVisual = Instantiate(abilityPrefab, offsetPosition, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
			abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 800);
			
			abilityVisual.GetComponent<Fireball>().damageModifier = 1 * currentDuration;
			abilityVisual.GetComponent<Fireball>().attackerID = attackerID;
			if (attackerID == 0) {
				abilityVisual.GetComponent<Fireball>().isClient = true;
			}
		}
	}
	
	
	
	private void ranged(Protocol.Command command, Vector2 position, Vector2 rotation,  float currentDuration, long attackerID) {
		
		if (command == Protocol.Command.Mouse0Down || command == Protocol.Command.Mouse1Down) {			
			
		} else if (command == Protocol.Command.Mouse0Up || command == Protocol.Command.Mouse1Up) {
			print (currentDuration);
			GameObject abilityPrefab = Resources.Load("Shared/Prefabs/Abilities/RangedStandard") as GameObject;
			Vector3 targetPos = new Vector3(rotation.x, rotation.y, 0);
			
			Vector3 offset = targetPos * 3;
			Vector3 offsetPosition = new Vector3(position.x + offset.x, position.y + offset.y, 0);
			
			try {
			gameObject.GetComponent<Animate>().BowAttack();
			} catch (Exception) {
				
			}
			
			GameObject abilityVisual = Instantiate(abilityPrefab, offsetPosition, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
			
			// Modify arrow speed
			if (currentDuration > 1.0) {
				currentDuration = 1.0f;
			}
			float adjustedSpeed = 0.4f + (currentDuration * 0.6f);			
			abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 3000 * adjustedSpeed);
			
			abilityVisual.GetComponent<RangedStandard>().damageModifier = 1 * currentDuration;
			abilityVisual.GetComponent<RangedStandard>().attackerID = attackerID;
			if (attackerID == 0) {
				abilityVisual.GetComponent<RangedStandard>().isClient = true;
			}
		}
	}
	
	
	
	private void multiShot(Protocol.Command command, Vector2 position, Vector2 rotation,  float currentDuration, long attackerID) {
		
		if (command == Protocol.Command.Mouse0Down || command == Protocol.Command.Mouse1Down) {			
			
		} else if (command == Protocol.Command.Mouse0Up || command == Protocol.Command.Mouse1Up) {
			print (currentDuration);
			GameObject abilityPrefab = Resources.Load("Shared/Prefabs/Abilities/RangedStandard") as GameObject;
			Vector3 targetPos = new Vector3(rotation.x, rotation.y, 0);
			
			Vector3 offset = targetPos * 3;
			Vector3 offsetPosition = new Vector3(position.x + offset.x, position.y + offset.y, 0);
			
			try {
			gameObject.GetComponent<Animate>().BowAttack();
			} catch (Exception) {
				
			}
			
			GameObject abilityVisual = Instantiate(abilityPrefab, offsetPosition, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
			GameObject abilityVisual2 = Instantiate(abilityPrefab, offsetPosition, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;
			
			abilityVisual.transform.eulerAngles = new Vector3(
                abilityVisual.transform.eulerAngles.x,
                abilityVisual.transform.eulerAngles.y,
                abilityVisual.transform.eulerAngles.z + 3f
            );
			abilityVisual2.transform.eulerAngles = new Vector3(
                abilityVisual2.transform.eulerAngles.x,
                abilityVisual2.transform.eulerAngles.y,
                abilityVisual2.transform.eulerAngles.z + -3f
            );
			
			// Modify arrow speed
			if (currentDuration > 1.0) {
				currentDuration = 1.0f;
			}
			float adjustedSpeed = 0.4f + (currentDuration * 0.6f);			
			abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 3000 * adjustedSpeed);
			abilityVisual2.rigidbody.AddForce(abilityVisual2.transform.up * 3000 * adjustedSpeed);
			
			
			abilityVisual.GetComponent<RangedStandard>().damageModifier = 1 * currentDuration;
			abilityVisual.GetComponent<RangedStandard>().attackerID = attackerID;
			if (attackerID == 0) {
				abilityVisual.GetComponent<RangedStandard>().isClient = true;
			}
			
			abilityVisual2.GetComponent<RangedStandard>().damageModifier = 1 * currentDuration;
			abilityVisual2.GetComponent<RangedStandard>().attackerID = attackerID;
			if (attackerID == 0) {
				abilityVisual2.GetComponent<RangedStandard>().isClient = true;
			}
		}
	}
	
	
	
	private void icyChill(Protocol.Command command, Vector2 position, Vector2 rotation,  float currentDuration, long attackerID) {
		
		if (command == Protocol.Command.Mouse0Down || command == Protocol.Command.Mouse1Down) {			
			
		} else if (command == Protocol.Command.Mouse0Up || command == Protocol.Command.Mouse1Up) {
			print (currentDuration);
			GameObject abilityPrefab = Resources.Load("Shared/Prefabs/Abilities/IcyChill") as GameObject;
			Vector3 targetPos = new Vector3(rotation.x, rotation.y, 0);
			
			Vector3 offset = targetPos * 4;
			Vector3 offsetPosition = new Vector3(position.x + offset.x, position.y + offset.y, 0);
			
			try {
			gameObject.GetComponent<Animate>().MeleeAttack();
			} catch (Exception) {
				
			}
			
			GameObject abilityVisual = Instantiate(abilityPrefab, offsetPosition, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;			
			abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 200);
			
			abilityVisual.GetComponent<IcyChill>().attackerID = attackerID;
			if (attackerID == 0) {
				abilityVisual.GetComponent<IcyChill>().isClient = true;
			}
		}
	}
	
	private void lightningBolt(Protocol.Command command, Vector2 position, Vector2 rotation,  float currentDuration, long attackerID) {
		
		if (command == Protocol.Command.Mouse0Down || command == Protocol.Command.Mouse1Down) {			
			
		} else if (command == Protocol.Command.Mouse0Up || command == Protocol.Command.Mouse1Up) {
			print (currentDuration);
			GameObject abilityPrefab = Resources.Load("Shared/Prefabs/Abilities/LightningBolt") as GameObject;
			Vector3 targetPos = new Vector3(rotation.x, rotation.y, 0);
			
			Vector3 offset = targetPos * 4;
			Vector3 offsetPosition = new Vector3(position.x + offset.x, position.y + offset.y, 0);
			
			try {
			gameObject.GetComponent<Animate>().MeleeAttack();
				} catch (Exception) {
				
			}
				
			GameObject abilityVisual = Instantiate(abilityPrefab, offsetPosition, Quaternion.LookRotation(Vector3.forward, targetPos)) as GameObject;			
			abilityVisual.rigidbody.AddForce(abilityVisual.transform.up * 1200);
			
			abilityVisual.GetComponent<LightningBolt>().attackerID = attackerID;
			if (attackerID == 0) {
				abilityVisual.GetComponent<LightningBolt>().isClient = true;
			}
		}
	}
//	
//	
//	
//	public void doMeleeAbility(Protocol.Ability ability, Protocol.Command command, Vector2 position, Vector2 rotation, float currentDuration, long attackerID) {
//		
//		switch(ability) {
//			
//		case Protocol.Ability.Melee :
//			melee(command, position, rotation, currentDuration, attackerID);
//			break;
//			
//		}
//			
//	}
}
