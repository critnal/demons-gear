using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour {
	
	public Transform parent;
	
	public float rotateSpeed = 540;

	// Use this for initialization
	void Start () {
		parent = transform.parent.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.RotateAround(parent.position, parent.transform.up, rotateSpeed * Time.fixedDeltaTime);
	}
}
