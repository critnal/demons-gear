using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hotbar : MonoBehaviour {
	
	CharacterAbilities characterAbilities;
		
	
	// HOTBAR STUFF
	private Texture2D
		hotbarBackground,
		hotbarForeground,
		hotbarLMBSelected,
		hotbarRMBSelected,
		hotbarMovement;
	
	public float 
		hotbarLeft = 10,
		hotbarTop = 10,
		hotbarWidth = 290,
		hotbarHeight = 56,
		hotbarKeyTop = 8,
		hotbarKeyLeft = 4,
		hotbarKeyWidth = 40,
		hotbarKeyHeight = 40,
		hotbarKeyGap = 8,
		hotbarSelectedLeft = 0,
		hotbarSelectedTop = 0,
		hotbarSelectedTopExtra = 16,
		hotbarSelectedWidth = 48,
		hotbarSelectedHeight = 72,
		hotbarSelectedLabelOffset = 11,
		hotbarMovementgap = 16;
	
	
	
	
	
	


	// Use this for initialization
	void Start () {
		
		characterAbilities = GetComponent<CharacterAbilities>();
		
		// HOTBAR STUFF
		hotbarBackground = new Texture2D(1, 1, TextureFormat.RGB24, false);
        hotbarForeground = new Texture2D(1, 1, TextureFormat.RGB24, false);
		hotbarLMBSelected = new Texture2D(1, 1, TextureFormat.RGB24, false);
        hotbarRMBSelected = new Texture2D(1, 1, TextureFormat.RGB24, false);
		hotbarMovement = new Texture2D(1, 1, TextureFormat.RGB24, false);
 
        hotbarBackground.SetPixel(0, 0, Color.grey);
        hotbarForeground.SetPixel(0, 0, Color.black);
		hotbarLMBSelected.SetPixel(0, 0, Color.red);
        hotbarRMBSelected.SetPixel(0, 0, Color.blue);
		hotbarMovement.SetPixel(0, 0, Color.yellow);
 	
        hotbarBackground.Apply();
        hotbarForeground.Apply();
		hotbarLMBSelected.Apply();
        hotbarRMBSelected.Apply();
		hotbarMovement.Apply();
	}
	
	
	
	void OnGUI() {		
		
		if (this.GetComponent<Character>().ID != Client.Me.ID) {
			return;
		}
		
		// HOTBAR SECTION
		float hotbarRepositionedTop = Screen.height - hotbarTop - hotbarHeight;
		int hasMovementAbility = (characterAbilities.equippedMovementAbility == Protocol.MovementAbility.Null) ? 0 : 1;
		hotbarWidth = hotbarKeyLeft + (characterAbilities.equippedAbilities.Count + hasMovementAbility) * (hotbarKeyWidth + hotbarKeyGap) + hotbarMovementgap * hasMovementAbility;
		
		
		
		// This draws the hotbar background		
		Rect hotbarBackgroundSection = new Rect(hotbarLeft, hotbarRepositionedTop, hotbarWidth, hotbarHeight);
		GUI.DrawTexture(hotbarBackgroundSection, hotbarBackground, ScaleMode.StretchToFill);
		
		
		
		
		// This draws the selected ability indicators
		Rect hotbarSelectedSection = new Rect(hotbarLeft, hotbarRepositionedTop - hotbarSelectedTopExtra, hotbarWidth, hotbarSelectedHeight);
		
		int selectedIndex = characterAbilities.lmbAbilityIndex;
		Rect hotbarLMBSelectedKey = new Rect(hotbarSelectedLeft + selectedIndex * hotbarSelectedWidth, hotbarSelectedTop, hotbarSelectedWidth, hotbarSelectedHeight);
		Rect hotbarLMBSelectedLabel = new Rect(hotbarSelectedLeft + hotbarSelectedLabelOffset + (selectedIndex * hotbarSelectedWidth), hotbarSelectedTop, hotbarSelectedWidth, hotbarSelectedHeight);
		
		selectedIndex = characterAbilities.rmbAbilityIndex;
		Rect hotbarRMBSelectedKey = new Rect(hotbarSelectedLeft + selectedIndex * hotbarSelectedWidth, hotbarSelectedTop, hotbarSelectedWidth, hotbarSelectedHeight);
		Rect hotbarRMBSelectedLabel = new Rect(hotbarSelectedLeft + hotbarSelectedLabelOffset + (selectedIndex * hotbarSelectedWidth), hotbarSelectedTop, hotbarSelectedWidth, hotbarSelectedHeight);
		
		GUI.BeginGroup(hotbarSelectedSection);
		{
			// This draws the LMB selected ability indicator
			GUI.DrawTexture(hotbarLMBSelectedKey, hotbarLMBSelected, ScaleMode.StretchToFill);
			GUI.Label(hotbarLMBSelectedLabel, "L");
			
			// This draws the RMB selected ability indicator
			GUI.DrawTexture(hotbarRMBSelectedKey, hotbarRMBSelected, ScaleMode.StretchToFill);
			GUI.Label(hotbarRMBSelectedLabel, "R");
		}
		GUI.EndGroup();
		
		
		
		
		
		
		// This draws each of the ability icons
		Rect hotbarAbilityIconsSection = new Rect(hotbarLeft, hotbarRepositionedTop, hotbarWidth, hotbarHeight);
		
		GUI.BeginGroup(hotbarAbilityIconsSection);
		{
			for (int i = 0; i < characterAbilities.equippedAbilities.Count; i++) {
				Texture2D tempIcon = Resources.Load("Client/Visuals/Textures/HotbarIcons/" + characterAbilities.equippedAbilities[i].ToString()) as Texture2D;
				Rect tempRect = new Rect(hotbarKeyLeft + i * (hotbarKeyWidth + hotbarKeyGap), hotbarKeyTop, hotbarKeyWidth, hotbarKeyHeight);
				
				GUI.DrawTexture(tempRect, tempIcon, ScaleMode.StretchToFill);
			}
		}
		GUI.EndGroup();
		
		
		
		
		
		// This draws the movement ability icon
		if (hasMovementAbility == 1) {
			// Bound key indicator
			Rect hotbarMovementIndicator = new Rect(hotbarWidth - hotbarKeyWidth - hotbarKeyLeft, hotbarRepositionedTop - hotbarSelectedTopExtra, hotbarSelectedWidth, hotbarSelectedHeight);
			
			GUI.DrawTexture(hotbarMovementIndicator, hotbarMovement, ScaleMode.StretchToFill);
			
			
			// Indicator label
			Rect hotbarMovementLabel = new Rect(hotbarWidth - hotbarKeyWidth - hotbarKeyLeft, hotbarRepositionedTop - hotbarSelectedTopExtra, hotbarSelectedWidth, hotbarSelectedHeight);
			
			GUI.Label(hotbarMovementLabel, "Space");
			
			
			// Icon
			Rect hotbarMovementIcon = new Rect(hotbarWidth - hotbarKeyWidth, hotbarRepositionedTop + hotbarKeyTop, hotbarKeyWidth, hotbarKeyHeight);
						
			Texture2D tempIcon = Resources.Load("Client/Visuals/Textures/HotbarIcons/" + characterAbilities.equippedMovementAbility.ToString()) as Texture2D;
			
			GUI.DrawTexture(hotbarMovementIcon, tempIcon, ScaleMode.StretchToFill);
		}
		
	}
}
