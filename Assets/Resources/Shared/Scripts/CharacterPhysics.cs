using UnityEngine;
using System.Collections;
using Lidgren.Network;

public class CharacterPhysics : MonoBehaviour {
	
	public byte IgnoreFrames = 5;
	public byte FrameWaitMax = 5;
	public float GravityModifier = 1000.0f;
	public float JumpSpeed = 20.0f;
	public float MovementSpeed = 15.0f;
	public float WallJumpSpeed = 9.0f;
	public float WallJumpHorizontalSpeed = 12.0f;
	public float AirMovementSpeed = 50f;
	public int WallGrabLength = 25;
	public float LedgeGrabLength = 0.2f;
	public float JumpHoldDuration = 0.2f;
	public float JumpEndTime = 0f;
	 public float MovementAcceleration = 9.0f;
	
	public bool LeftDown = false;
	public bool RightDown = false;
	public bool UpDown = false;
	public bool DownDown = false;
	
	private float GroundDistance;
	private float ArmDistance;
	
	private bool UseGravity = true;
	
	private byte LeftWait = 255;
	private byte RightWait = 255;
	private byte GrabWait = 255;
	private int WallWait = 255;
	
	public bool isDoingMovementAbility = false;
	
	void Start () {
		GroundDistance = collider.bounds.extents.y;
		ArmDistance = collider.bounds.extents.x;
		WallWait = WallGrabLength;
	}
	
	//For rigidbody
	void FixedUpdate () {
		
		if (isDoingMovementAbility) {
			return;
		}
		
		UseGravity = true;
		
		if (IsGrounded()) {
			if (LeftDown && !RightDown) {
			    // Grounded and moving left
			    rigidbody.velocity = new Vector3(Mathf.Clamp(rigidbody.velocity.x + (-MovementSpeed * (MovementAcceleration * Time.fixedDeltaTime)), -MovementSpeed, 0), rigidbody.velocity.y, 0);
			    gameObject.GetComponent<Animate>().moveDirection = -Vector2.right;
			   } else if (RightDown && !LeftDown) {
			    // Grounded and moving right
			    rigidbody.velocity = new Vector3(Mathf.Clamp(rigidbody.velocity.x + (MovementSpeed * (MovementAcceleration * Time.fixedDeltaTime)), 0, MovementSpeed), rigidbody.velocity.y, 0);
			    gameObject.GetComponent<Animate>().moveDirection = Vector2.right;
			   } else {
			    // Grounded but not moving
			    rigidbody.velocity = new Vector3(0, rigidbody.velocity.y, 0);
			    gameObject.GetComponent<Animate>().moveDirection = Vector2.zero;
			   }
			
			if (UpDown) {
				if (IsTouchingRightWall() || IsTouchingLeftWall()) {
					GrabWait = 0;
				} else {
					// Grounded and jumping
					rigidbody.velocity = new Vector3(rigidbody.velocity.x, JumpSpeed, 0);
					JumpEndTime = Time.time + JumpHoldDuration;
				}
			}
		} else if (IsTouchingRightWall() && rigidbody.velocity.y >= -2.4f) {
			if (GrabWait >= IgnoreFrames) {
				if (WallWait == WallGrabLength + 1) {
					WallWait = 0;
				}
				if (UpDown) {
					if (LeftDown && !RightDown) {
						// Touching right wall above ground pressing left and jump
						rigidbody.velocity = new Vector3(-WallJumpHorizontalSpeed, WallJumpSpeed, 0);
						LeftWait = 255;
						WallWait = WallGrabLength + 1;
						JumpEndTime = Time.time + JumpHoldDuration;
					} else if (RightDown && !LeftDown) {
						// Touching right wall above ground pressing right and jump
						if (IsOnRightLedge()) {
							rigidbody.velocity = new Vector3(0, WallJumpSpeed, 0);
							GrabWait = 0;
							WallWait = WallGrabLength + 1;
							UpDown = false;
						} else {
							if (WallWait < WallGrabLength) {
								rigidbody.velocity = Vector3.right;
								UseGravity = false;
							} else {
								rigidbody.velocity = new Vector3(1, -1, 0);
								UseGravity = true;//change
							}
							UpDown = false;
						}
					} else {
						// Touching right wall above ground pressing jump
						if (LeftWait <= FrameWaitMax) {
							rigidbody.velocity = new Vector3(-MovementSpeed, WallJumpSpeed / 2, 0);
							LeftWait = 255;
							WallWait = WallGrabLength + 1;
							UpDown = false;
						} else {
							if (IsOnRightLedge()) {
								rigidbody.velocity = new Vector3(0, WallJumpSpeed, 0);
								GrabWait = 0;
								WallWait = WallGrabLength + 1;
								UpDown = false;
							} else {
								if (WallWait < WallGrabLength) {
									rigidbody.velocity = Vector3.right;
									UseGravity = false;
								} else {
									rigidbody.velocity = new Vector3(1, -1, 0);
									UseGravity = true;
								}
							}
							//Normal Jump
						}
					}
				} else {
					if (LeftDown && !RightDown) {
						// Touching right wall above ground pressing left
						if (LeftWait > FrameWaitMax) {
							LeftWait = 0;
						} else if (LeftWait == FrameWaitMax){
							rigidbody.velocity = new Vector3(-MovementSpeed / 2, WallJumpSpeed / 6, 0);
							LeftWait = 255;
							GrabWait = 0;
							WallWait = WallGrabLength + 1;
							Debug.Log("Left Wait Timeout");
						} else {
							rigidbody.velocity = new Vector3(0, 0, 0);
							UseGravity = false;
						}
					} else if (RightDown && !LeftDown) {
						// Touching right wall above ground pressing right
						if (WallWait < WallGrabLength) {
							rigidbody.velocity = Vector3.right;
							UseGravity = false;
						} else {
							rigidbody.velocity = new Vector3(1, -1, 0);
							UseGravity = true;//change
						} 
					} else if (DownDown) {
						rigidbody.velocity = new Vector3(-MovementSpeed / 6, WallJumpSpeed / 8, 0);
						RightWait = 255;
						GrabWait = 0;
						WallWait = WallGrabLength + 1;
					} else {
						// Touching right wall above ground
						if (WallWait < WallGrabLength) {
							rigidbody.velocity = Vector3.right;
							UseGravity = false;
						} else {
							rigidbody.velocity = new Vector3(1, -1, 0);
							UseGravity = true;//change
						}
					}
				}
			}
		} else if (IsTouchingLeftWall() && rigidbody.velocity.y >= -2.4f) {
			if (GrabWait >= IgnoreFrames) {
				if (WallWait == WallGrabLength) {
					WallWait = 0;
				}
				if (UpDown) {
					if (LeftDown && !RightDown) {
						// Touching left wall above ground pressing left and jump
						if (IsOnLeftLedge()) {
							rigidbody.velocity = new Vector3(0, WallJumpSpeed, 0);
							GrabWait = 0;
							WallWait = WallGrabLength + 1;
							UpDown = false;
						} else {
							if (WallWait < WallGrabLength) {
								rigidbody.velocity = Vector3.left;
								UseGravity = false;
							} else {
								rigidbody.velocity = new Vector3(-1, -1, 0);
								UseGravity = true;//change
							}
						}
					} else if (RightDown && !LeftDown) {
						// Touching left wall above ground pressing right and jump
						rigidbody.velocity = new Vector3(WallJumpHorizontalSpeed, WallJumpSpeed, 0);
						RightWait = 255;
						WallWait = WallGrabLength + 1;
						JumpEndTime = Time.time + JumpHoldDuration;
					} else {
						// Touching left wall above ground pressing jump
						if (RightWait <= FrameWaitMax) {
							rigidbody.velocity = new Vector3(MovementSpeed, WallJumpSpeed / 2, 0);
							RightWait = 255;
							WallWait = WallGrabLength + 1;
							UpDown = false;
						} else {
							if (IsOnLeftLedge()) {
								rigidbody.velocity = new Vector3(0, WallJumpSpeed, 0);
								GrabWait = 0;
								WallWait = WallGrabLength + 1;
								UpDown = false;
							} else {
								if (WallWait < WallGrabLength) {
									rigidbody.velocity = Vector3.left;
									UseGravity = false;
								} else {
									rigidbody.velocity = new Vector3(-1, -1, 0);
									UseGravity = true;//change
								}
							}
							//Normal Jump
						}
					}
				} else {
					if (LeftDown && !RightDown) {
						// Touching left wall above ground pressing left
						if (WallWait < WallGrabLength) {
							rigidbody.velocity = Vector3.left;
							UseGravity = false;
						} else {
							rigidbody.velocity = new Vector3(-1, -1, 0);
							UseGravity = true;//change
						}
					} else if (RightDown && !LeftDown) {
						// Touching left wall above ground pressing right
						if (RightWait > FrameWaitMax) {
							RightWait = 0;
						} else if (RightWait == FrameWaitMax){
							rigidbody.velocity = new Vector3(MovementSpeed / 2, WallJumpSpeed / 6, 0);
							RightWait = 255;
							GrabWait = 0;
							WallWait = WallGrabLength + 1;
							Debug.Log("Right Wait Timeout");
						} else {
							rigidbody.velocity = new Vector3(0, 0, 0);
							UseGravity = false;
						}
					} else if (DownDown) {
						rigidbody.velocity = new Vector3(MovementSpeed / 6, WallJumpSpeed / 8, 0);
						RightWait = 255;
						GrabWait = 0;
						WallWait = WallGrabLength + 1;
					} else {
						// Touching left wall above ground
						if (WallWait < WallGrabLength) {
							rigidbody.velocity = Vector3.left;
							UseGravity = false;
						} else {
							rigidbody.velocity = new Vector3(-1, -1, 0);
							UseGravity = true;//change
						}
					}
				}
			}
		} else {
			
			if (LeftDown && !RightDown) {
				// In air and moving left
				rigidbody.velocity = new Vector3(-AirMovementSpeed <= rigidbody.velocity.x? (-AirMovementSpeed * 2) + rigidbody.velocity.x : rigidbody.velocity.x, rigidbody.velocity.y, 0);
			} else if (RightDown && !LeftDown) {
				// In air and moving right
				rigidbody.velocity = new Vector3(AirMovementSpeed >= rigidbody.velocity.x? (AirMovementSpeed * 2) + rigidbody.velocity.x : rigidbody.velocity.x, rigidbody.velocity.y, 0);
			} else {
				// In air but not moving
			}
			
			if (UpDown && Time.time <= JumpEndTime) {
				// In air and jumping
				rigidbody.velocity = new Vector3(rigidbody.velocity.x, JumpSpeed, 0);
			}
		}
		
		if (LeftWait < FrameWaitMax) {
			LeftWait++;
		} else if (LeftWait == FrameWaitMax) {
			rigidbody.velocity = new Vector3(-MovementSpeed / 2, WallJumpSpeed / 6, 0);
			LeftWait = 255;
			UseGravity = true;
		}
		
		if (RightWait < FrameWaitMax) {
			RightWait++;
		} else if (RightWait == FrameWaitMax) {
			rigidbody.velocity = new Vector3(MovementSpeed / 2, WallJumpSpeed / 6, 0);
			RightWait = 255;
			UseGravity = true;
		}
		
		if (GrabWait < IgnoreFrames) {
			GrabWait++;
		}
		
		if (WallWait < WallGrabLength) {
			WallWait++;
		}
		
		if (UseGravity) {
			rigidbody.AddForce(Physics.gravity * rigidbody.mass * GravityModifier);
		}
		
		gameObject.GetComponent<Animate>().onGround = gameObject.GetComponent<CharacterPhysics>().IsGrounded();
		
	}
	
	public bool IsGrounded() {
		return Physics.Raycast(transform.position, -Vector3.up, GroundDistance + 0.1f);
	}
	
	bool IsGrounded(out RaycastHit hit) {
		return Physics.Raycast(transform.position, -Vector3.up, out hit, GroundDistance + 0.1f);
	}
	
	bool IsGrounded(int layerMask) {
		return Physics.Raycast(transform.position, -Vector3.up, GroundDistance + 0.1f, layerMask);
	}
	
	bool IsGrounded(int layerMask, out RaycastHit hit) {
		return Physics.Raycast(transform.position, -Vector3.up, out hit, GroundDistance + 0.1f, layerMask);
	}
	
	
	bool IsOnRightLedge() {
		Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + GroundDistance + LedgeGrabLength, transform.position.z), Vector3.right,Color.red, ArmDistance + 0.1f);
		return !Physics.Raycast(new Vector3(transform.position.x, transform.position.y + GroundDistance + LedgeGrabLength, transform.position.z), Vector3.right, ArmDistance + 0.1f);
	}
	
	bool IsOnLeftLedge() {
		return !Physics.Raycast(new Vector3(transform.position.x, transform.position.y + GroundDistance + LedgeGrabLength, transform.position.z), Vector3.left, ArmDistance + 0.1f);
	}
	
	bool IsTouchingRightWall() {
		return Physics.Raycast(transform.position, Vector3.right, ArmDistance + 0.1f);
	}
	
	bool IsTouchingLeftWall() {
		return Physics.Raycast(transform.position, Vector3.left, ArmDistance + 0.1f);
	}
	
	
	public void MakeMovementPacket(ref NetOutgoingMessage msg) {
		
		msg.Write(transform.position.x);
		msg.Write(transform.position.y);
		
		msg.Write(rigidbody.velocity.x);
		msg.Write(rigidbody.velocity.y);
		//is jumping? velocity? position?
	}
	
	public void UseMovementPacket(ref NetIncomingMessage msg, ref double networkTime, ref Vector3 networkPosition, ref Vector3 networkVelocity) {
		//interpolation
		
		float x = msg.ReadFloat();
		float y = msg.ReadFloat();
		
		float xvel = msg.ReadFloat();
		float yvel = msg.ReadFloat();
		
		networkTime = msg.SenderConnection.GetRemoteTime(msg.ReceiveTime);
		networkPosition = new Vector3(x, y, 0);
		networkVelocity = new Vector3(xvel, yvel, 0);
		
//		Debug.Log("Pos: " + x + " : " + y + " Vel: " + xvel + " : " + yvel + " Network offset: " + networkTime);
	}	
}
