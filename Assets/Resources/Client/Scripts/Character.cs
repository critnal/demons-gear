using UnityEngine;
using System.Collections;
using Lidgren.Network;

public class Character : MonoBehaviour
{	
	public long ID;
	public bool Mine = false;
	public int HP = 500;
	public int MaxHp = 500;
	
	
	private double networkTime = 0.0;
	private Vector3 networkPosition = Vector3.zero;
	private Vector3 networkVelocity = Vector3.zero;
	
	//Changes
	
	public double m_InterpolationBackTime = 0.1;
	public double m_ExtrapolationLimit = 0.5;
	
	
	internal struct  State
	{
		internal double timestamp;
		internal Vector3 pos;
		internal Vector3 velocity;
	}
	
	State[] m_BufferedState = new State[20];
	int m_TimestampCount;
	
	void Start ()
	{
	}
	
	
	
	void Update ()
	{
		double interpolationTime = Client.client.ServerConnection.GetRemoteTime (NetTime.Now) - m_InterpolationBackTime;
		
		// Use interpolation if the target playback time is present in the buffer

		if (m_BufferedState [0].timestamp > interpolationTime) {

			// Go through buffer and find correct state to play back

			for (int i=0; i<m_TimestampCount; i++) {

				if (m_BufferedState [i].timestamp <= interpolationTime || i == m_TimestampCount - 1) {

					// The state one slot newer (<100ms) than the best playback state
					State rhs = m_BufferedState [Mathf.Max (i - 1, 0)];

					// The best playback state (closest to 100 ms old (default time))
					State lhs = m_BufferedState [i];

     

					// Use the time between the two slots to determine if interpolation is necessary
					double length = rhs.timestamp - lhs.timestamp;
					float t = 0.0F;

					// As the time difference gets closer to 100 ms t gets closer to 1 in 
					// which case rhs is only used

					// Example:
					// Time is 10.000, so sampleTime is 9.900 
					// lhs.time is 9.910 rhs.time is 9.980 length is 0.070
					// t is 9.900 - 9.910 / 0.070 = 0.14. So it uses 14% of rhs, 86% of lhs

					if (length > 0.0001) {
						t = (float)((interpolationTime - lhs.timestamp) / length);
					}

     				if (lhs.pos.x > rhs.pos.x) {
						gameObject.GetComponent<Animate>().moveDirection = -Vector2.right;
					} else if (lhs.pos.x < rhs.pos.x) {
						gameObject.GetComponent<Animate>().moveDirection = Vector2.right;
					} else {
						gameObject.GetComponent<Animate>().moveDirection = Vector2.zero;
					}

					// if t=0 => lhs is used directly
					transform.localPosition = Vector3.Lerp (lhs.pos, rhs.pos, t);
					//transform.localRotation = Quaternion.Slerp (lhs.rot, rhs.rot, t);
					return;

				}

			}

		} else {

			State latest = m_BufferedState [0];
			
			float extrapolationLength = (float)(interpolationTime - latest.timestamp);

			// Don't extrapolation for more than 500 ms, you would need to do that carefully
			if (extrapolationLength < m_ExtrapolationLimit) {

				//float axisLength = extrapolationLength * latest.angularVelocity.magnitude * Mathf.Rad2Deg;
				//Quaternion angularRotation = Quaternion.AngleAxis (axisLength, latest.angularVelocity);

				rigidbody.position = latest.pos + latest.velocity * extrapolationLength;
				//rigidbody.rotation = angularRotation * latest.rot;
				rigidbody.velocity = latest.velocity;
				//rigidbody.angularVelocity = latest.angularVelocity;

			}
		}
		
		
		
	}
	
	void LateUpdate() {
		/*if (IsMe) {
			Debug.Log("HAI");
			Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, camera.transform.position.z);
		}	*/
	}
	
	public void UseSyncPacket (ref NetIncomingMessage msg)
	{
		gameObject.GetComponent<CharacterPhysics> ().UseMovementPacket (ref msg, ref networkTime, ref networkPosition, ref networkVelocity);
		
		for (int i=m_BufferedState.Length-1; i>=1; i--) {
			m_BufferedState [i] = m_BufferedState [i - 1];
		}
		
		State state;
		state.timestamp = networkTime;
		state.pos = networkPosition;
		state.velocity = networkVelocity;
		m_BufferedState [0] = state;		
		
		// Update used slot count, however never exceed the buffer size
		// Slots aren't actually freed so this just makes sure the buffer is
		// filled up and that uninitalized slots aren't used.

		m_TimestampCount = Mathf.Min (m_TimestampCount + 1, m_BufferedState.Length);

		// Check if states are in order, if it is inconsistent you could reshuffel or 
		// drop the out-of-order state. Nothing is done here

		for (int i=0; i<m_TimestampCount-1; i++) {

			if (m_BufferedState [i].timestamp < m_BufferedState [i + 1].timestamp) {
				Debug.Log ("State inconsistent");
			}
		} 

	}
	
}
